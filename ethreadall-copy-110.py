####################################################################################################
#BigJob program to submit multiple jobs to multiple VMs in EC2
#Uses multi-threading to submit each program to each VM that will be created simultaneously
#Program-1-csblast
#Program-2-pftools
#Program-3-hmmer
#Program-4-psipred+threader
#Program-5-hhpred
#Program-6-sparks
#Program-7-sp3
#Program-8-compass
#Program-9-samt2k
#Program-10-pgenthreader
#go()-executes each program by spawning threads
#Program-11-ethread (works after all the programs complete execution)
####################################################################################################
import threading
import os
import time,datetime
import sys
import logging
import uuid
import json
import shutil
from pprint import pprint

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

class mainThread:
    def __init__(self, cfg_file):
        self.running = True

        if  not 'ETHREAD_ACCESS_KEY_ID' in os.environ :
            print 'ACCESS KEY ID not set in environment'
        if not 'ETHREAD_SECRET_ACCESS_KEY' in os.environ :
            print 'SECRET ACCESS KEY not set in environment'
        if not 'COORDINATION_URL' in os.environ :
            print 'Coordination URL not set in environment'

        #Load data in the cfg file using json
        json_file = open (cfg_file)
        json_data = ''.join(json_file.readlines())
        json_data = json_data % os.environ
        self._cfg_data  = json.loads(json_data)
#        pprint (self._cfg_data)
        #print len(self._cfg_data['programs'])
        #print len(self._cfg_data['program_to_omit'])
        
        '''#------------------------------------------------------------------------------
        # Redis password and eThread secret key details aquired from the environment
        COORD = str(self._cfg_data['coordination_url'])
        ACCESS_KEY_ID = self._cfg_data['pcd_common']['access_key_id']
        SECRET_ACCESS_KEY= self._cfg_data['pcd_common']['secret_access_key']
        #------------------------------------------------------------------------------'''
        
        # create pilot data service (factory for data pilots (physical, distributed storage))
        # and pilot data
        ###################################################################################################
        ##Pilot-data Service
        ###################################################################################################
        print "\nSetting up Pilot Data Service in S3"
        
        self.pilot_data_service = PilotDataService(coordination_url = COORD)
        self.pilot_data_description_aws={
                                        "service_url": "s3://test_110dat_ab-" + str(uuid.uuid1()),
                                        "size": 100,
                                        "access_key_id": ACCESS_KEY_ID,
                                        "secret_access_key":SECRET_ACCESS_KEY
                                   }
        self.pd = self.pilot_data_service.create_pilot(pilot_data_description=self.pilot_data_description_aws)
        
        #-------------------------------------------------------------------------------------------------#
        # Put all input files, that will be upload  to S3 
        input_files = []
        for files in self._cfg_data['input_file_path']:
            input_files.append(os.path.join(os.getcwd(), str(files)))
        data_unit_description = { "file_urls": input_files }
        print input_files
        #-------------------------------------------------------------------------------------------------#
        #get the gene names into a variable
        self.genes = self._cfg_data['dataset']
        """print genes
        dataset_count = 0
        for dataset in genes:
             dataset_count = dataset_count + 1
             print dataset  + "\t"
        print dataset_count"""
        #-------------------------------------------------------------------------------------------------#
        
        # Load input data into PD(PD for AWS is S3) 
        self.input_data_unit = self.pd.submit_data_unit(data_unit_description)
        self.input_data_unit.wait()
        logger.info("Input Data Unit URL: " + self.input_data_unit.get_url())
        print "\nData Unit URL: " + self.input_data_unit.get_url()
#        print "\nInput Files:\n ", self.input_data_unit.list()
#        sys.exit (0)
        #---------------------------------------------------------------------------------------------------
        # create place holder for output data 
        output_data_unit_description = { "file_urls": [] }
        self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
        self.output_data_unit.wait()
        #---------------------------------------------------------------------------------------------------
        #Location for all output files locally
        self.op_path=os.getcwd()+str(self._cfg_data['output_file_path'])
#        print self.op_path
        if os.path.exists(self.op_path):
            shutil.rmtree(self.op_path)
        os.mkdir(self.op_path,0755)
#        sys.exit (0)
        #########################################################################################################
        self.pilot_compute_service = PilotComputeService(coordination_url=COORD)
        #########################################################################################################

    ##########################################################################################################
    ##Program-1 - CSBLAST
    ##########################################################################################################
    def pgm_csblast(self):
        print "\ncsblast: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['csblast']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['csblast']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
#        print pilot_compute_description_amazon_west1
#        sys.exit (0)
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("csblast.txt","a")
        print "\ncsblast: Time to set up VM1: ", tot_time1
        fo.write("\ncsblast: Time to set up VM1: %s\n" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-1-CS BLAST: compute unit starts here"
        # create compute unit-chain-part1
        for dataset in self.genes:
            start_time = time.time()
            compute_unit_descriptionc1 = {
                "working_directory": str(self._cfg_data['cud_common']['working_directory']),
#                "executable": "csblast-2.1.0-linux64/csblast_static",
                "executable": "/bin/echo",
#                "arguments": [" -i "+str(dataset)+".fasta -d /home/ethread/libs/ethread-lib-latest/blast/chain -D /home/ethread/apps/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /home/ethread/apps/blast-2.2.25/bin -o" +str(dataset)+"-chain.csblast"],
                "arguments": [" I am task c1 of " +str(dataset)],
                "number_of_processes": 1,
                "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
                "output_data": [
                        {
                                self.output_data_unit.get_url():
#                                ["std*"+str(dataset)+"*",str(dataset)+"*.csblast"]
                                ["std*"]
                        }
                 ],
                "output": "stdout_"+str(dataset)+"_chain_blast.txt",
                "error": "stderr_"+str(dataset)+"_chain_blast.txt"
            }
            compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
            compute_unitc1.wait()
            logger.info("csblast-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print "csblast-chain-part1:"+dataset+" Finished setup of ComputeDataService. Waiting for scheduling of PD" 

            # create compute unit-domain-part1
            compute_unit_descriptiond1 = {
                "working_directory": str(self._cfg_data['cud_common']['working_directory']),
#                "executable": "/home/ethread/apps/csblast-2.1.0-linux64/csblast_static",
                "executable": "/bin/echo",
#                "arguments": [" -i "+str(dataset)+".fasta -d /home/ethread/libs/ethread-lib-latest/blast/domain -D /home/ethread/apps/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /home/ethread/apps/blast-2.2.25/bin -o" +str(dataset)+"-domain.csblast"],
                "arguments": [" I am task d1 of " +str(dataset)],
#                "environment": {"ds": str(dataset)},
                "number_of_processes": 1,
                "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
                "output_data": [
                        {
                                self.output_data_unit.get_url():
#                                ["std*"+str(dataset)+"*",str(dataset)+"*.csblast"]
                                ["std*"]
                        }
                 ],
                "output": "stdout_"+str(dataset)+"_domain_blast.txt",
                "error": "stderr_"+str(dataset)+"_domain_blast.txt"
            }
            compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
            compute_unitd1.wait()
            self.output_data_unit.wait()
            logger.info("csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print "csblast-doamin-part1:"+dataset+ " Finished setup of ComputeDataService. Waiting for scheduling of PD"
            print "csblast: part-1: output files:\t %s\n" %self.output_data_unit.list()

#            pilotjob1.wait()
            tot_time = time.time() - start_time
            #print "csblast: part-1: input files:\t %s\n" %self.input_data_unit.list()
            print "csblast: part-1: output files:\t %s\n" %self.output_data_unit.list()
            fo=open("csblast.txt","a")
            print "\ncsblast: Time for "+dataset+" part 1: ", tot_time
            fo.write("\ncsblast: Time for "+dataset+ " part 1: %s\n" %tot_time)
            fo.close()
            #-------------------------------------------------------------------------------------------------#
            # Start file converting after finishing job, if it is needed
            # Compute Unit Description12 - CS BLAST: Part-2
            #-------------------------------------------------------------------------------------------------#
            start_time = time.time()
            # create compute unit-chain-part2
            compute_unit_descriptionc2 = {
                "working_directory": str(self._cfg_data['cud_common']['working_directory']),
#                "executable": "ethread-1.0/bin/econv-csiblast",
                "executable": "/bin/echo",
#                "arguments": ["-i "+str(dataset)+"-chain.csblast -o "+str(dataset)+"-chain.csblast.ali"],
                "arguments": [" I am task c2 of " +str(dataset)],
                "number_of_processes": 1,
                "input_data": [self.output_data_unit.get_url()],
                "output_data": [
                        {
                                self.output_data_unit.get_url():
#                                ["std*"+str(dataset)+"*",str(dataset)+"*.ali"]
                                ["std*"]
                        }
                ],
                "output": "stdout_"+str(dataset)+"_chain_blast_final.txt",
                "error": "stderr_"+str(dataset)+"_chain_blast_final.txt"
            }
            compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
            compute_unitc2.wait()
            logger.info("csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print "csblast-chain-Part-2: "+dataset+" Finished setup of ComputeDataService. Waiting for scheduling of PD"

            # create compute unit-domain-part2
            compute_unit_descriptiond2 = {
                "working_directory": str(self._cfg_data['cud_common']['working_directory']),
#                "executable": "ethread-1.0/bin/econv-csiblast",
                "executable": "/bin/echo",
#                "arguments": ["-i "+str(dataset)+"-domain.csblast -o "+str(dataset)+"-domain.csblast.ali"],
                "arguments": [" I am task d2 of " +str(dataset)],
                "number_of_processes": 1,
                "input_data": [self.output_data_unit.get_url()],
                "output_data": [
                        {
                                self.output_data_unit.get_url():
#                                ["std*"+str(dataset)+"*",str(dataset)+"*.ali"]
                                ["std*"]
                        }
                ],
                "output": "stdout_"+str(dataset)+"_domain_blast_final.txt",
                "error": "stderr_"+str(dataset)+"_domain_blast_final.txt"
            }

            compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
            compute_unitd2.wait()
            logger.info("csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print "csblast: Part-2: "+dataset+" Finished setup of ComputeDataService. Waiting for scheduling of PD"

            self.output_data_unit.wait()
#            pilotjob1.wait()
            tot_time = time.time() - start_time
            #print "csblast: part-2: input files:\t %s\n" %self.input_data_unit.list()
            #print "csblast: part-2: output files:\t %s\n" %self.output_data_unit.list()
            fo=open("csblast.txt","a")
            print "\ncsblast: Time for "+dataset+" part 2: ", tot_time
            fo.write("\ncsblast: Time for "+dataset+" part 2: %s" %tot_time)
            fo.close()
        print "\ncsblast: exporting du to local"
        self.output_data_unit.wait()
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("csblast: Terminate Pilot Compute/Data Service")
        print "\ncsblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-2 - pftools
    ##########################################################################################################
    def pgm_pftools(self):
        print "\npftools: Creating VM instance"
        start_time = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west2 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['pftools']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['pftools']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob2 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        fo=open("pftools.txt","a")
        print "\npftools: Time to set up VM2: ", tot_time
        fo.write("\npftools: Time to set up VM2: %s" %tot_time)
        fo.close
        #----------------------------------------------------------------------------------
        #Cumpute Unit Description - program pftools part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-2-pf tools: compute unit starts here"
        start_time = time.time()
        # create compute unit-pftools-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/pftools/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /home/ethread/libs/ethread-lib-latest/pftools/chain.prf > F6VMN7-chain.pftools"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_chain_pftools.txt",
            "error": "stderr_chain_pftools.txt"
        }
        compute_unitc1 = pilotjob2.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/usr/bin/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /home/ethread/libs/ethread-lib-latest/pftools/domain.prf > F6VMN7-domain.pftools"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_domain_pftools.txt",
            "error": "stderr_domain_pftools.txt"
        }
        compute_unitd1 = pilotjob2.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("pftools-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "pftools: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "pftools: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("pftools.txt","a")
        print "\npftools: Time for part 1: ", tot_time
        fo.write("\npftools: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - pftools: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-pftools-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-chain.pftools -o F6VMN7-chain.pftools.ali -l /home/ethread/libs/ethread-lib-latest/pftools/chain.map"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_pftools_final.txt",
            "error": "stderr_chain_pftools_final.txt"
        }
        compute_unitc2 = pilotjob2.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-domain.pftools -o F6VMN7-domain.pftools.ali -l /home/ethread/libs/ethread-lib-latest/pftools/domain.map"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_pftools_final.txt",
            "error": "stderr_domain_pftools_final.txt"
        }

        compute_unitd2 = pilotjob2.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob2.wait()

        tot_time = time.time() - start_time
        fo=open("pftools.txt","a")
        print "\npftools: Time for part 2: ", tot_time
        fo.write("\npftools: Time for part 2: %s" %tot_time)
        fo.close()
        print "\npftools: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("pftools: Terminate Pilot Compute/Data Service")
        print "\npftools: Terminate Pilot Compute/Data Service"
        pilotjob2.cancel()
       ################################################################################################################

    ##########################################################################################################
    ##Program-3 - hmmer
    
    ##########################################################################################################
    def pgm_hmmer(self):
        print "\nHMMER: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['hmmer']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['hmmer']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("HMMER.txt","a")
        print "\nHMMER: Time to set up VM1: ", tot_time1
        fo.write("\nHMMER: Time to set up VM1: %s\n" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program hmmer part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-1-HMMER: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hmmer-3.0-linux-intel-x86_64/src/hmmscan",
            "arguments": ["--cpu 1 -o F6VMN7-chain.hmmer --notextw -E 100.0 --max /home/ethread/libs/ethread-lib-latest/hmmer/chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_HMMER_blast.txt",
            "error": "stderr_HMMER_blast.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
	pilotjob1.wait()
        logger.info("HMMER-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "HMMER-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"
	
        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hmmer-3.0-linux-intel-x86_64/src/hmmscan ",
            "arguments": ["--cpu 1 -o F6VMN7-domain.hmmer --notextw -E 100.0 --max /home/ethread/libs/ethread-lib-latest/hmmer/domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_domain_HMMER.txt",
            "error": "stderr_domain_HMMER.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("HMMER-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "HMMER-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "HMMER: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "HMMER: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("HMMER.txt","a")
        print "\nHMMER: Time for part 1: ", tot_time
        fo.write("\nHMMER: Time for part 1: %s\n" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - hmmer: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-chain.hmmer -o F6VMN7-chain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_HMMER_final.txt",
            "error": "stderr_chain_HMMER_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
	pilotjob1.wait()
        logger.info("HMMER-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "HMMER-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-domain.hmmer -o F6VMN7-domain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_HMMER_final.txt",
            "error": "stderr_domain_HMMER_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("HMMER: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "HMMER: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "HMMER: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "HMMER: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("HMMER.txt","a")
        print "\nHMMER: Time for part 2: ", tot_time
        fo.write("\nHMMER: Time for part 2: %s" %tot_time)
        fo.close()
        print "\nHMMER: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("HMMER: Terminate Pilot Compute/Data Service")
        print "\nHMMER: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-4 - psipred and Threader
    ##########################################################################################################
    def pgm_psipred_threader(self):
        print "\npsipred: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['psipred']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['psipred']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob4 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("psipred-threader.txt","a")
        print "\npsipred: Time to set up VM4: ", tot_time1
        fo.write("\npsipred: Time to set up VM4: %s" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program psipred part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-4-psipred: compute unit starts here"
        start_time = time.time()
        # create compute unit
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/psipred321/runpsipred",
            "arguments": [" F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_psipred.txt",
            "error": "stderr_psipred.txt"
        }
        compute_unit = pilotjob4.submit_compute_unit(compute_unit_description)
        logger.info("psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "psipred: input files:\t %s\n" %self.input_data_unit.list()
        print "psipred: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("psipred-threader.txt","a")
        print "\npsipred: Time for psipred: ", tot_time
        fo.write("\npsipred: Time for psipred: %s" %tot_time)
        fo.close()
        pilotjob4.cancel()
        #********************************************************************************************#
        # Start Threader after psipred                                                               #
        #********************************************************************************************#
        print "\nProgram-4-Threader part: compute unit starts here"

        start_time = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['threader']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['threader']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob4 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time = time.time() - start_time
        fo=open("psipred-threader.txt","a")
        print "\nThreader: Time to set up VM4: ", tot_time1
        fo.write("\nThreader: Time to set up VM4: %s" %tot_time1)
        fo.close()
        #********************************************************************************************#
        # Compute Unit Description12 - threader: Part-2                                              #
        # create compute unit-chain-part1                                                            #
        #********************************************************************************************#
        start_time = time.time()
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-chain.threader.out /home/ethread/libs/ethread-lib-latest/threader/chain.lst > F6VMN7-chain.threader"],
            "environment": ["THREAD_DIR=/home/ethread/apps/threader","TDB_DIR=/home/ethread/libs/ethread-lib-latest/threader/chain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_chain_threader.txt",
            "error": "stderr_chain_threader.txt"
        }
        compute_unitc1 = pilotjob4.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-domain.threader.out /home/ethread/libs/ethread-lib-latest/threader/domain.lst > F6VMN7-domain.threader"],
            "environment": ["THREAD_DIR=/home/ethread/apps/threader","TDB_DIR=/home/ethread/libs/ethread-lib-latest/threader/domain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_domain_threader.txt",
            "error": "stderr_domain_threader.txt"
        }
        compute_unitd1 = pilotjob4.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "threader: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("psipred-threader.txt","a")
        print "\nthreader: Time for part 1: ", tot_time
        fo.write("\nthreader: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-chain.threader.out -a F6VMN7-chain.threader -o F6VMN7-chain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_chain_threader_final.txt",
            "error": "stderr_chain_threader_final.txt"
        }
        compute_unitc2 = pilotjob4.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-domain.threader.out -a F6VMN7-domain.threader -o F6VMN7-domain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_domain_threader_final.txt",
            "error": "stderr_domain_threader_final.txt"
        }

        compute_unitd2 = pilotjob4.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "threader: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("psipred-threader.txt","a")
        print "\nthreader: Time for part 2: ", tot_time
        fo.write("\nthreader: Time for part 2: %s" %tot_time)
        fo.close()
        print "\npsipred-threader: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("psipred-threader: Terminate Pilot Compute/Data Service")
        print "\npsipred-threader: Terminate Pilot Compute/Data Service"
        pilotjob4.cancel()
        ###################################################################################################

##########################################################################################################
    ##Program-5 - hhpred 
    ##########################################################################################################
    def pgm_hhpred(self):
        print "\nhhpred: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['hhpred']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['hhpred']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob5 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("hhpred.txt","a")
        print "\nhhpred: Time to set up VM5: ", tot_time1
        fo.write("\nhhpred: Time to set up VM5: %s" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program HHPRED part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-hhpred: compute unit starts here"
        start_time = time.time()
        # create compute unit-for perl part
        compute_unit_description1 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "perl",
            "arguments": ["/hh_1.5.0.linux64/buildali.pl -fas F6VMN7.fasta"],
            "environment": ["Data=/home/ethread/apps/blast-2.2.25/data","PERL5LIB=$PERL5LIB:/home/ethread/apps/hh_1.5.0.linux64"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_perl_hhpred.txt",
            "error": "stderr_perl_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        logger.info("hhpred-perl-part: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-perl-part: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unit1.wait()

        # create compute unit-hhm-part1
        compute_unit_description1 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hh_1.5.1.1.linux32/hhmake",
            "arguments": [" -i F6VMN7.a3m"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_hhm1_hhpred.txt",
            "error": "stderr_hhm1_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        compute_unit1.wait()

        # create compute unit-hhm-part2
        compute_unit_description1 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -cal -i F6VMN7.hhm -d /home/ethread/libs/ethread-lib-latest/libs/hhpred/cal.hhm"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_hhm2_hhpred.txt",
            "error": "stderr_hhm2_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        logger.info("hhpred-hhm-part: Finished setup of Computeunit. Waiting for scheduling of PD")
        print "hhpred-hhm-part: Finished setup of Computeunit. Waiting for scheduling of PD"
        compute_unit1.wait()

        #**************************************************************************
        #HHPRED CHAIN & DOMAIN PART 1                                             *
        #**************************************************************************
        # create compute unit-hhpred-chain-part1
        compute_unit_descriptionc1 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -i F6VMN7.hhm -d /home/ethread/libs/ethread-lib-latest/hhpred/chain.hhm -o F6VMN7-chain.hhpred -p 1.0"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["*hhpred*"]
                        }
                 ],
            "output": "stdout_chain_hhpred.txt",
            "error": "stderr_chain_hhpred.txt"
        }
        compute_unitc1 = pilotjob5.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("hhpred-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hhpred-domain-part1
        compute_unit_descriptiond1 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -i F6VMN7.hhm -d /home/ethread/libs/ethread-lib-latest/hhpred/domain.hhm -o F6VMN7-domain.hhpred -p 1.0"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["*hhpred*"]
                        }
                 ],
            "output": "stdout_domain_hhpred.txt",
            "error": "stderr_domain_hhpred.txt"
        }
        compute_unitd1 = pilotjob5.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("hhpred-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob5.wait()
        tot_time = time.time() - start_time
        print "hhpred: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "hhpred: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("hhpred.txt","a")
        print "\nhhpred: Time for part 1: ", tot_time
        fo.write("\nhhpred: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - hhpred: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-hhpred",
            "arguments": [" -i F6VMN7-chain.hhpred -o F6VMN7-chain.hhpred.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_hhpred_final.txt",
            "error": "stderr_chain_hhpred_final.txt"
        }
        compute_unitc2 = pilotjob5.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("hhpred-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            #"working_directory": "/root/testing",
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-hhpred",
            "arguments": ["-i F6VMN7-domain.hhpred -o F6VMN7-domain.hhpred.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_hhpred_final.txt",
            "error": "stderr_domain_hhpred_final.txt"
        }

        compute_unitd2 = pilotjob5.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("hhpred: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob5.wait()
        tot_time = time.time() - start_time
        print "hhpred: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "hhpred: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("hhpred.txt","a")
        print "\nhhpred: Time for part 2: ", tot_time
        fo.write("\nhhpred: Time for part 2: %s" %tot_time)
        fo.close()
        print "\nhhpred: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("hhpred: Terminate Pilot Compute/Data Service")
        print "\nhhpred: Terminate Pilot Compute/Data Service"
        pilotjob5.cancel()
        ###################################################################################################
    ##########################################################################################################
    ##Program-6 - sparks
    ##########################################################################################################
    def pgm_sparks(self):
        print "\nsparks: Creating VM instance"
        start_time = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west2 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['sparks']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['sparks']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob6 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        fo=open("sparks.txt","a")
        print "\nsparks: Time to set up VM: ", tot_time
        fo.write("\nsparks: Time to set up VM: %s" %tot_time)
        fo.close()
        #----------------------------------------------------------------------------------
        #Cumpute Unit Description - program sparks part-1
        #----------------------------------------------------------------------------------
        print "\nsparks: compute unit starts here"
        start_time = time.time()
        # create compute unit-sparks-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sh",
            "arguments": [" sparks-chain.sh"],
            "environment": ["sparks=/home/ethread/apps/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_sparks.txt",
            "error": "stderr_chain_sparks.txt"
        }
        compute_unitc1 = pilotjob6.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("sparks-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sparks-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sh",
            "arguments": [" sparks-domain.sh"],
            "environment": ["sparks=/home/ethread/apps/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_sparks.txt",
            "error": "stderr_domain_sparks.txt"
        }
        compute_unitd1 = pilotjob6.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("sparks-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob6.wait()
        tot_time = time.time() - start_time
        print "sparks: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "sparks: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("sparks.txt","a")
        print "\nsparks: Time for part 1: ", tot_time
        fo.write("\nsparks: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - sparks: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-sparks-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-sparks",
            "arguments": ["-s F6VMN7-chain.spk2.out -a F6VMN7-chain.spk2 -o F6VMN7-chain.sparks2.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_sparks_final.txt",
            "error": "stderr_chain_sparks_final.txt"
        }
        compute_unitc2 = pilotjob6.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("sparks-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sparks-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-sparks",
            "arguments": ["-s F6VMN7-domain.spk2.out -a F6VMN7-domain.spk2 -o F6VMN7-domain.sparks2.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_sparks_final.txt",
            "error": "stderr_domain_sparks_final.txt"
        }

        compute_unitd2 = pilotjob6.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("sparks-domain: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-domain: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob6.wait()

        tot_time = time.time() - start_time
        fo=open("sparks.txt","a")
        print "\nsparks: Time for part 2: ", tot_time
        fo.write("\nsparks: Time for part 2: %s" %tot_time)
        fo.close()
        print "\nsparks: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("sparks: Terminate Pilot Compute/Data Service")
        print "\nsparks: Terminate Pilot Compute/Data Service"
        pilotjob6.cancel()
    ###########################################################################################################################

    ##########################################################################################################
    ##Program-7 - sp3
    ##########################################################################################################
    def pgm_sp3(self):
        print "\nsp3: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['sp3']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['sp3']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob7 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("sp3.txt","a")
        print "\nsp3: Time to set up VM: ", tot_time1
        fo.write("\nsp3: Time to set up VM: %s" %tot_time1)
        fo.close()
        #-------------------------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program sp3 part-1
        #-------------------------------------------------------------------------------------------------
        print "\nsp3: compute unit starts here"
        start_time = time.time()
        # create compute unit-sp3-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sh",
            "arguments": [" sp3-chain.sh"],
            "environment": ["sparks=/home/ethread/apps/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_sp3.txt",
            "error": "stderr_chain_sp3.txt"
        }
        compute_unitc1 = pilotjob7.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("sp3-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sp3-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sh",
            "arguments": [" sp3-domain.sh"],
            "environment": ["sparks=/home/ethread/apps/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_sp3.txt",
            "error": "stderr_domain_sp3.txt"
        }
        compute_unitd1 = pilotjob7.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("sp3-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob7.wait()
        tot_time = time.time() - start_time
        print "sp3: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "sp3: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("sp3.txt","a")
        print "\nsp3: Time for part 1: ", tot_time
        fo.write("\nsp3: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - sp3: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-sp3-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s F6VMN7-chain.sp3.out -a F6VMN7-chain.sp3 -o F6VMN7-chain.sp3.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_sp3_final.txt",
            "error": "stderr_chain_sp3_final.txt"
        }
        compute_unitc2 = pilotjob7.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("sp3-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sp3-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s F6VMN7-domain.sp3.out -a F6VMN7-domain.sp3 -o F6VMN7-domain.sp3.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_sp3_final.txt",
            "error": "stderr_domain_sp3_final.txt"
        }

        compute_unitd2 = pilotjob7.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("sp3: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob7.wait()
        tot_time = time.time() - start_time
        print "sp3: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "sp3: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("sp3.txt","a")
        print "\nsp3: Time for part 2: ", tot_time
        fo.write("\nsp3: Time for part 2: %s" %tot_time)
        fo.close()
        print "\nsp3: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("sp3: Terminate Pilot Compute/Data Service")
        print "\nsp3: Terminate Pilot Compute/Data Service"
        pilotjob7.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-8 - compass
    ##########################################################################################################
    def pgm_compass(self):
        print "\ncompass: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['compass']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['compass']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob8 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("compass.txt","a")
        print "\ncompass: Time to set up VM: ", tot_time1
        fo.write("\ncompass: Time to set up VM: %s" %tot_time1)
        fo.close()

        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program compass part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-compass-blast part: compute unit starts here"
        start_time = time.time()
        # create compute unit for blast part-1
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/blast-2.2.25/bin/blastpgp",
            "arguments": [" -i F6VMN7.fasta -d /home/ethread/libs/ethread-lib-latest/libs/uniprot/uniref90 -h 0.001 -j 5 -m 6 -o F6VMN7.blastout"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.blastout"]
                        }
                 ],
            "output": "stdout_compass-blast-1.txt",
            "error": "stderr_compass-blast-1.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        pilotjob8.wait()
        print "compass-blast: input files:\t %s\n" %self.input_data_unit.list()
        print "compass-blast-part-1: output files:\t %s\n" %self.output_data_unit.list()

        # create compute unit for blast part-2
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "perl",
            "arguments": [" /compass-3.1/get_last_br_iter_1.pl -i F6VMN7.blastout -o F6VMN7.aln1 -c /home/ethread/apps/compass-3.1/prep_psiblastali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.aln1"]
                        }
                 ],
            "output": "stdout_compass-blast-2.txt",
            "error": "stderr_compass-blast-2.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        pilotjob8.wait()
        print "compass-blast-part-2: output files:\t %s\n" %self.output_data_unit.list()

        # create compute unit for blast part-3
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-fix1",
            "arguments": [" F6VMN7.aln1 F6VMN7.aln"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.aln"]
                        }
                 ],
            "output": "stdout_compass-blast-3.txt",
            "error": "stderr_compass-blast-3.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        pilotjob8.wait()
        print "compass-blast: output files:\t %s\n" %self.output_data_unit.list()

        
        tot_time = time.time() - start_time
        fo=open("compass.txt","a")
        print "\nblast: Time for blast part: ", tot_time
        fo.write("\nblast: Time for blast part: %s" %tot_time)
        fo.close()
        #********************************************************************************************#
        # Start compass after blast part                                                             #
        # Compute Unit Description12 - compass: Part-2                                               #
        #********************************************************************************************#
        print "\nProgram-compass part: compute unit starts here"
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i F6VMN7.aln -d /home/ethread/libs/ethread-lib-latest/compass/chain.db -o F6VMN7-chain.compass"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.compass"]
                        }
                 ],
            "output": "stdout_chain_compass.txt",
            "error": "stderr_chain_compass.txt"
        }
        compute_unitc1 = pilotjob8.submit_compute_unit(compute_unit_descriptionc1)
	pilotjob8.wait()
        logger.info("compass-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i F6VMN7.aln -d /home/ethread/libs/ethread-lib-latest/compass/domain.db -o F6VMN7-domain.compass"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.compass"]
                        }
                 ],
            "output": "stdout_domain_compass.txt",
            "error": "stderr_domain_compass.txt"
        }
        compute_unitd1 = pilotjob8.submit_compute_unit(compute_unit_descriptiond1)
	pilotjob8.wait()
        logger.info("compass-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        
        tot_time = time.time() - start_time
        print "compass: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "compass: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("compass.txt","a")
        print "\ncompass: Time for part 1: ", tot_time
        fo.write("\ncompass: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - compass: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-compass",
            "arguments": [" -i F6VMN7-chain.compass -o F6VMN7-chain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_compass_final.txt",
            "error": "stderr_chain_compass_final.txt"
        }
        compute_unitc2 = pilotjob8.submit_compute_unit(compute_unit_descriptionc2)
	pilotjob8.wait()
        logger.info("compass-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-compass",
            "arguments": [" -i F6VMN7-domain.compass -o F6VMN7-domain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_compass_final.txt",
            "error": "stderr_domain_compass_final.txt"
        }

        compute_unitd2 = pilotjob8.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("compass: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob8.wait()
        tot_time = time.time() - start_time
        print "compass: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "compass: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("compass.txt","a")
        print "\ncompass: Time for part 2: ", tot_time
        fo.write("\ncompass: Time for part 2: %s" %tot_time)
        fo.close()
        print "\ncompass: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("compass: Terminate Pilot Compute/Data Service")
        print "\ncompass: Terminate Pilot Compute/Data Service"
        pilotjob8.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-9 - samt2k
    ##########################################################################################################
    def pgm_samt2k(self):
        print "\nsamt2k: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['samt2k']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['samt2k']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob9 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("samt2k.txt","a")
        print "\nsamt2k: Time to set up VM: ", tot_time1
        fo.write("\nsamt2k: Time to set up VM: %s" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program samt2k part-1
        #----------------------------------------------------------------------------------
        print "\ntarget2k: compute unit starts here"
        start_time = time.time()
        # create compute unit for target2k part-1
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/target2k",
            "arguments": [" -seed F6VMN7.fasta -out F6VMN7-t2k"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-1.txt",
            "error": "stderr_target2k-1.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-1"
        compute_unit.wait()
        # create compute unit for target2k part-2
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/w0.5",
            "arguments": [" F6VMN7-t2k.a2m F6VMN7-t2k-w0.5.mod"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-2.txt",
            "error": "stderr_target2k-2.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-2"
        compute_unit.wait()
        # create compute unit for target2k part-3
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" F6VMN7-t2k -i F6VMN7-t2k-w0.5.mod -calibrate 1"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-3.txt",
            "error": "stderr_target2k-3.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-3"
        compute_unit.wait()
        # create compute unit for target2k part-4
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sed",
            "arguments": [" -e s/insert\ /model_file\ /g F6VMN7-t2k.mlib > F6VMN7-t2k.fix | mv F6VMN7-t2k.fix F6VMN7-t2k.mlib"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { self.output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-4.txt",
            "error": "stderr_target2k-4.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-4"
        compute_unit.wait()

        print "target2k: input files:\t %s\n" %self.input_data_unit.list()
        print "target2k: output files:\t %s\n" %self.output_data_unit.list()

        tot_time = time.time() - start_time
        fo=open("samt2k.txt","a")
        print "\nsamt2k: Time for target2k part: ", tot_time
        fo.write("\nsamt2k: Time for target2k part: %s" %tot_time)
        fo.close()
        #********************************************************************************************#
        # Start samt2k after target2k part                                                           #
        # Compute Unit Description12 - samt2k: Part-2                                                #
        #********************************************************************************************#
        print "\nProgram-samt2k-part: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": ["chain -modellibrary F6VMN7-t2k.mlib -db /home/ethread/libs/ethread-lib-latest/blast/chain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5 | mv chain.1.F6VMN7-t2k-w0.5.mod.a2m F6VMN7-chain-t2k-w0.5.a2m | mv chain.1.F6VMN7-t2k-w0.5.mod.dist F6VMN7-chain-t2k-w0.5.dist"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_samt2k.txt",
            "error": "stderr_chain_samt2k.txt"
        }
        compute_unitc1 = pilotjob9.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("samt2k-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" domain -modellibrary F6VMN7-t2k.mlib -db /home/ethread/libs/ethread-lib-latest/blast/domain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5 | mv domain.1.F6VMN7-t2k-w0.5.mod.a2m F6VMN7-domain-t2k-w0.5.a2m | mv domain.1.F6VMN7-t2k-w0.5.mod.dist F6VMN7-domain-t2k-w0.5.dist"],
            "environment": ["BLA_FILE=/home/ethread/test/F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_samt2k.txt",
            "error": "stderr_domain_samt2k.txt"
        }
        compute_unitd1 = pilotjob9.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("samt2k-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob9.wait()
        tot_time = time.time() - start_time
        print "samt2k: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "samt2k: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("samt2k.txt","a")
        print "samt2k\n: Time for part 1: ", tot_time
        fo.write("samt2k\n: Time for part 1: %s" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - samt2k: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" F6VMN7-chain-t2k-w0.5.a2m -l 10000 > F6VMN7-chain-t2k-w0.5.aln"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_chain_samt2k_2.txt",
            "error": "stderr_chain_samt2k_2.txt"
        }
        compute_unitc2 = pilotjob9.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("samt2k-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unitc2.wait()

        # create compute unit-chain-part3
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t F6VMN7.fasta -s F6VMN7-chain-t2k-w0.5.dist -a F6VMN7-chain-t2k-w0.5.aln -o F6VMN7-chain.samt2k.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_chain_samt2k_final.txt",
            "error": "stderr_chain_samt2k_final.txt"
        }
        compute_unitc2 = pilotjob9.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("samt2k-chain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" F6VMN7-domain-t2k-w0.5.a2m -l 10000 > F6VMN7-domain-t2k-w0.5.aln"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_domain_samt2k_2.txt",
            "error": "stderr_domain_samt2k_2.txt"
        }
       
        compute_unitd2 = pilotjob9.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("samt2k-domain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-domain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unitd2.wait()

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t F6VMN7.fasta -s F6VMN7-domain-t2k-w0.5.dist -a F6VMN7-domain-t2k-w0.5.aln -o F6VMN7-domain.samt2k.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_domain_samt2k_final.txt",
            "error": "stderr_domain_samt2k_final.txt"
        }

        compute_unitd2 = pilotjob9.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("samt2k-domain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-domain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob9.wait()
        tot_time = time.time() - start_time
        print "samt2k: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "samt2k: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("samt2k.txt","a")
        print "\nsamt2k: Time for part 2: ", tot_time
        fo.write("\nsamt2k: Time for part 2: %s" %tot_time)
        fo.close()
        print "\nsamt2k: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("samt2k: Terminate Pilot Compute/Data Service")
        print "\nsamt2k: Terminate Pilot Compute/Data Service"
        pilotjob9.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-10 - pgenthreader
    ##########################################################################################################
    #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$#
    def pgm_pgen(self):
        print "\npgen pilotjobpgenthreader: Creating VM instance"
        start_time1 = time.time()
        json_parse = self._cfg_data['pcd_common']
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['pgen']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['pgen']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjobpgen = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("pgen.txt","a")
        print "\npgen: Time to set up VM: ", tot_time1
        fo.write("\npgen: Time to set up VM: %s" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program pgenthreader part-1
        #----------------------------------------------------------------------------------
        print "\ncompute unit starts here"
        start_time = time.time()
        
        # create compute unit for blast part-1
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/pgen/pGenThreader-chain.sh",
            "arguments": [" pgt-F6VMN7-chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","pgt-F6VMN7*"]
                        }
                 ],
            "output": "stdout_pgen-chain-1.txt",
            "error": "stderr_pgen-chain-1.txt"
        }
        compute_unit = pilotjobpgen.submit_compute_unit(compute_unit_description)
        compute_unit.wait()

        # create compute unit for blast part-2
        compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/pgen/pGenThreader-domain.sh",
            "arguments": [" pgt-F6VMN7-domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","pgt-F6VMN7*"]
                        }
                 ],
            "output": "stdout_pgen-domain-1.txt",
            "error": "stderr_pgen-domain-1.txt"
        }
        compute_unit = pilotjobpgen.submit_compute_unit(compute_unit_description)
        compute_unit.wait()

        tot_time = time.time() - start_time
        print "\ncompass: Time for execution part: ", tot_time
        #********************************************************************************************#
        # Start compass after blast part                                                             #
        # Compute Unit Description12 - compass: Part-2                                               #
        #********************************************************************************************#
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-pgenthreader",
            "arguments": ["-s pgt-F6VMN7-chain.pgen.presults -a pgt-F6VMN7-chain.pgen.align -o F6VMN7-chain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain2_pgen.txt",
            "error": "stderr_chain2_pgen.txt"
        }
        compute_unitc1 = pilotjobpgen.submit_compute_unit(compute_unit_descriptionc2)

        # create compute unit-domain-part1
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/ethread-1.0/bin/econv-pgenthreader",
            "arguments": [" -s pgt-F6VMN7-domain.pgen.presults -a pgt-F6VMN7-domain.pgen.align -o F6VMN7-domain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain2_pgen.txt",
            "error": "stderr_domain2_pgen.txt"
        }
        compute_unitd1 = pilotjobpgen.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("pgen-doamin-part2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pgen-doamin-part2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjobpgen.wait()
        tot_time = time.time() - start_time
        print "\ncompass: Time for part 2: ", tot_time
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("pgen: Terminate Pilot Compute/Data Service")
        print "\npgen: Terminate Pilot Compute/Data Service"
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - compass: Part-2
        #-------------------------------------------------------------------------------------------------#
        pilotjobpgen.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-11 - BLAST
    ##########################################################################################################
    def pgm_blast(self):
        print "\nblast: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['blast']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['blast']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        print pilot_compute_description_amazon_west1
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("blast.txt","a")
        print "\nblast: Time to set up VM1: ", tot_time1
        fo.write("\nblast: Time to set up VM1: %s\n" %tot_time1)
        fo.close()
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        #----------------------------------------------------------------------------------
        print "\nProgram BLAST: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "blast-2.2.25/bin/blastall",
            "arguments": ["  -a 2 -p blastp -d /home/ethread/libs/ethread-lib-latest/libs/ncbi/nr -i F6VMN7.fasta -e 400 -I -v 20000 -b 0 -o F6VMN7.blastall"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.blastall"]
                        }
                 ],
            "output": "stdout_blastall.txt",
            "error": "stderr_blastall.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("blast: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "blast: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i F6VMN7.fasta -d /home/ethread/libs/ethread-lib-latest/blast/domain -D /home/ethread/apps/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /home/ethread/apps/blast-2.2.25/bin -o F6VMN7-domain.csblast"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.csblast"]
                        }
                 ],
            "output": "stdout_domain_blast.txt",
            "error": "stderr_domain_blast.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "csblast: part-1: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("csblast.txt","a")
        print "\ncsblast: Time for part 1: ", tot_time
        fo.write("\ncsblast: Time for part 1: %s\n" %tot_time)
        fo.close()
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-chain.csblast -o F6VMN7-chain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_blast_final.txt",
            "error": "stderr_chain_blast_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-domain.csblast -o F6VMN7-domain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_blast_final.txt",
            "error": "stderr_domain_blast_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-2: input files:\t %s\n" %self.input_data_unit.list()
        print "csblast: part-2: output files:\t %s\n" %self.output_data_unit.list()
        fo=open("csblast.txt","a")
        print "\ncsblast: Time for part 2: ", tot_time
        fo.write("\ncsblast: Time for part 2: %s" %tot_time)
        fo.close()
        print "\ncsblast: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("csblast: Terminate Pilot Compute/Data Service")
        print "\ncsblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ###########################################################################################################################

        #***************************************************************************
        #*All the 11 programs can be written before the start of the go() function * 
        #*Essentially by copying the pgm_csblast() and modifying it as required    *
        #***************************************************************************

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called, s3cmd collects the data to local sys
    ########################################################################################################
    def go(self):
        threads=[]

        if not 'csblast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_csblast)
            thread.start()
        if not 'pftools' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_pftools)
            thread.start()
        if not 'hmmer' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_hmmer)
            thread.start()
        if not 'pgen' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_pgen)
            thread.start()
        if not 'hhpred' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_hhpred)
            thread.start()
        if not 'compass' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_compass)
            thread.start()
        if not 'samt2k' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_blast)
            thread.start()
            thread.join()
            thread=threading.Thread(target=self.pgm_samt2k)
            thread.start()
        if not 'psipred' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_psipred_threader)
            thread.start()
        if not 'sparks' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_sparks)
            thread.start()
        if not 'sp3' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_sp3)
            thread.start()

            threads.append(thread)

        for thread in threads:
            thread.join()
    #########################################################################################################            
        
    ####################################################################################################################
    #ethread starts here
    ####################################################################################################################
    ##########################################################################################################
    ##Program- ethread
    ##########################################################################################################
    def pgm_ethread(self):
        print "\nethread: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['blast']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['blast']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        
       
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-1-ethread: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            "arguments": ["F6VMN7-chain.compass.ali F6VMN7-chain.csblast.ali F6VMN7-chain.hhpred.ali F6VMN7-chain.hmmer.ali F6VMN7-chain.pftools.ali F6VMN7-chain.pgenthreader.ali F6VMN7-chain.samt2k.ali F6VMN7-chain.sp3.ali F6VMN7-chain.sparks2.ali F6VMN7-chain.threader.ali > F6VMN7-chain.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7-chain.ali"]
                        }
                 ],
            "output": "stdout_chain_blast.txt",
            "error": "stderr_chain_blast.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("ethread-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            "arguments": [" F6VMN7-domain.compass.ali F6VMN7-domain.csblast.ali F6VMN7-domain.hhpred.ali F6VMN7-domain.hmmer.ali F6VMN7-domain.pftools.ali F6VMN7-domain.pgenthreader.ali F6VMN7-domain.samt2k.ali F6VMN7-domain.sp3.ali F6VMN7-domain.sparks2.ali F6VMN7-domain.threader.ali > F6VMN7-domain.ali"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7-domain.ali"]
                        }
                 ],
            "output": "stdout_domain_blast.txt",
            "error": "stderr_domain_blast.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("ethread-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "ethread: part-1: input files:\t %s\n" %self.input_data_unit.list()
        print "ethread: part-1: output files:\t %s\n" %self.output_data_unit.list()
        
        
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - ethread: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            "environment": ["ET_THREADMOD=/usr/local/ethread-1.0/mod","ET_BINLIBSVM=/usr/local/libsvm-3.11/svm-predict"],
            
            "arguments": ["F6VMN7-chain.ali | /usr/local/ethread-1.0/bin/ethread -t F6VMN7.fasta -l /usr/local/libraries/ethread-lib-latest/blast/chain -o F6VMN7-chain.ethread -d chain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7-chain.ethread"]
                        }
                ],
            "output": "stdout_chain_blast_final.txt",
            "error": "stderr_chain_blast_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            "arguments": ["F6VMN7-domain.ali | /usr/local/ethread-1.0/bin/ethread -t F6VMN7.fasta -l /usr/local/libraries/ethread-lib-latest/blast/domain -o F6VMN7-domain.ethread -d domain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7-domain.ethread"]
                        }
                ],
            "output": "stdout_domain_blast_final.txt",
            "error": "stderr_domain_blast_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("ethread: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob1.wait()
        
        
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - ethread: Part-3
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            
            
            "arguments": ["F6VMN7-chain.ethread F6VMN7-domain.ethread > F6VMN7.ethread"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7.ethread"]
                        }
                ],
            "output": "stdout_chain_blast_final.txt",
            "error": "stderr_chain_blast_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        logger.info("ethread: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob1.wait()
        
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description3 - ethread: Part-3
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            
            
            "arguments": ["F6VMN7-chain.ali | /usr/local/ethread-1.0/bin/ethread -t F6VMN7.fasta -l /usr/local/libraries/ethread-lib-latest/blast/chain -o F6VMN7-chain.ethread -f F6VMN7-chain.ethread-fun -d chain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7.*"]
                        }
                ],
            "output": "stdout_chain_ethread_final.txt",
            "error": "stderr_chain_ethread_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "cat",
            "arguments": ["F6VMN7-domain.ali | /usr/local/ethread-1.0/bin/ethread -t F6VMN7.fasta -l /usr/local/libraries/ethread-lib-latest/blast/domain -o F6VMN7-domain.ethread -f F6VMN7-domain.ethread-fun -d domain"],
            "number_of_processes": 1,
            "input_data": [self.output_data_unit.get_url()],
            "output_data": [
                        {
                                self.output_data_unit.get_url():
                                ["std*","F6VMN7.*"]
                        }
                ],
            "output": "stdout_domain_blast_final.txt",
            "error": "stderr_domain_blast_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("ethread: Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "ethread: Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        self.output_data_unit.wait()
        pilotjob1.wait()
        
        
                
        print "\ncsblast: exporting du to local:"
        self.pd.export_du(self.output_data_unit,self.op_path)
        logger.info("ethread: Terminate Pilot Compute/Data Service")
        print "\nethread: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################


    
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
       

        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        #print "\nExport S3 data to local\n"
        #e = os.system('s3cmd get -r %s' %self.pd.service_url)

        #if not e == 0:
         #   print >> sys.stderr, 'Error on fetching data:', e
        #self.pd.export_du(self.pd,os.getcwd())
        #print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

        #delete the s3 bucket if you do not want to keep in s3
        #e = os.system('s3cmd rb -rf %s' %self.pd.service_url)

        #cancel pilot compute/data services
        self.pilot_data_service.cancel() 
        self.pilot_compute_service.cancel()
             
#################################################################################################################################
if __name__ == "__main__":

    #check for config file input 
    if len(sys.argv) != 2:
        sys.exit('Usage: %s <cfg-file>' % sys.argv[0])

    #Object to the class mainThread() with the config file as an arg
    obj = mainThread(sys.argv[1])
    #To call threads
    obj.go()
    #ethread will execute after all threads complete
    #obj.pgm_ethread()

