eThread SAGA-Pilot Module README
================================

This is the README file to set parameters and execute eThread program

USAGE
=====

To execute the eThread pythn program, type the following command on the terminal: 

> `$ python ethread-aws-ec2.py config.cfg`

JSON Config File Parameters
----------------------------

Required parameters are predominantly set through config.cfg JSON file

Explanation of parameters to be set:

* "programs": It is a list of threading tools for reference

* "program _ to _ omit": Here enter the threading tools that needn't be executed

* "coordination _ url": It is provided as an envoronment variable COORDINATION _ URL

* "pcd _ common": A dictionary of Pilot Compute Description parameters that are common throughout the program

> * "service _ url"       : Provide the AWS EC2 URL

> * "number _ of _ processes: Total number of cores available for running the tasks in the pilot

> * "vm _ ssh _ username"   : Provide the EC2 ssh login username credential

> * "vm _ ssh _ keyname"    : Provide the keyname used for ssh login

> * "vm _ ssh _ keyfile"    : Provide the location of the ssh keyfile used for authentication for AWS(could be a pem or rsa file)

> * "region"            : Priovide the region of the AWS EC2

> * "access _ key _ id"     : Secret access ID for login to AWS. Provided as environment variable ACCESS _ KEY _ ID

> * "secret _ access _ key" : Secret access password for login to AWS. Provided as environment variable SECRET _ ACCESS _ KEY

* "vm _ id": Each threading tool has an AMI. vm _ id is a dictionary of the AMI IDs for different threading tools

* "vm _ type": A dictionary of the type of instance to be used for each threading tool

* "input _ file _ path" : Provide the list of the location of the initial input files needed

* "dataset": Provide the list of the sequence names (without the .fasta extension) that are to be processed

* "output _ file _ path": Provide the local location and name of the directory to be created for placing the output files

* "cud _ common": A dictionary of Compute Unit Description parameters that are common throughout all CUDs

> * "working _ directory": Provide the location where the tasks are to be ececuted on EC2 instances
    

