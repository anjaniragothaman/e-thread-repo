'''
This is the README file to set parameters and execute eThread program
'''

#Syntax to execute the eThread program
python ethread-aws-ec2.py config.cfg

#Required parameters are predominantly set through config.cfg JSON file

#Explanation of parameters to be set
"programs": It is a list of threading tools for reference

"program_to_omit": Here enter the threading tools that needn't be executed

"coordination_url": It is provided as an envoronment variable COORDINATION_URL

"pcd_common": A dictionary of Pilot Compute Description parameters that are common throughout the program
    "service_url"       : Provide the AWS EC2 URL
    "number_of_processes: Total number of cores available for running the tasks in the pilot
    "vm_ssh_username"   : Provide the EC2 ssh login username credential
    "vm_ssh_keyname"    : Provide the keyname used for ssh login
    "vm_ssh_keyfile"    : Provide the location of the ssh keyfile used for authentication for AWS(could be a pem or rsa file)
    "region"            : Priovide the region of the AWS EC2
    "access_key_id"     : Secret access ID for login to AWS. Provided as environment variable ACCESS_KEY_ID
    "secret_access_key" : Secret access password for login to AWS. Provided as environment variable SECRET_ACCESS_KEY

"vm_id": Each threading tool has an AMI. vm_id is a dictionary of the AMI IDs for different threading tools

"vm_type": A dictionary of the type of instance to be used for each threading tool

"input_file_path" : Provide the list of the location of the initial input files needed

"dataset": Provide the list of the sequence names (without the .fasta extension) that are to be processed

"output_file_path": Provide the local location and name of the directory to be created for placing the output files

"cud_common": A dictionary of Compute Unit Description parameters that are common throughout all CUDs
    "working_directory": Provide the location where the tasks are to be ececuted on EC2 instances
    

