####################################################################################################
#This is a BigJob program to submit multiple threading tool tasks to multiple VMs in EC2
#Uses multi-threading to submit each threading tool to each VM that will be created simultaneously
#Program-1-csblast
#Program-2-pftools
#Program-3-hmmer
#Program-4-psipred+threader
#Program-5-hhpred
#Program-6-sparks
#Program-7-sp3
#Program-8-compass
#Program-9-samt2k
#Program-10-pgenthreader
#go()-executes each program by spawning threads
#Program-11-ethread (works after all the programs complete execution)
####################################################################################################
import threading
import os
import time,datetime
import sys
import logging
import pilot
import uuid
import json
import shutil
from pprint import pprint
from collections import deque

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

class mainThread:
    def __init__(self, cfg_file):
        self.running = True

        if  not 'ETHREAD_ACCESS_KEY_ID' in os.environ :
            print 'ACCESS KEY ID not set in environment'
        if not 'ETHREAD_SECRET_ACCESS_KEY' in os.environ :
            print 'SECRET ACCESS KEY not set in environment'
        if not 'COORDINATION_URL' in os.environ :
            print 'Coordination URL not set in environment'

        #Load data in the cfg file using json
        json_file = open (cfg_file)
        json_data = ''.join(json_file.readlines())
        json_data = json_data % os.environ
        self._cfg_data  = json.loads(json_data)
#        pprint (self._cfg_data)
        #print len(self._cfg_data['programs'])
        #print len(self._cfg_data['program_to_omit'])
        
        '''#------------------------------------------------------------------------------
        # Redis password and eThread secret key details aquired from the environment
        COORD = str(self._cfg_data['coordination_url'])
        ACCESS_KEY_ID = self._cfg_data['pcd_common']['access_key_id']
        SECRET_ACCESS_KEY= self._cfg_data['pcd_common']['secret_access_key']
        #------------------------------------------------------------------------------'''
        
        # create pilot data service (factory for data pilots (physical, distributed storage))
        # and pilot data
        ###################################################################################################
        ##Pilot-data Service
        ###################################################################################################
        print "\nSetting up Pilot Data Service in S3"
        
        self.pilot_data_service = PilotDataService(coordination_url = COORD)
        self.pilot_data_description_aws={
                                        "service_url": "s3://anj-pgen-test" + str(uuid.uuid1()),
                                        "size": 100,
                                        "access_key_id": ACCESS_KEY_ID,
                                        "secret_access_key":SECRET_ACCESS_KEY
                                   }
        self.pd = self.pilot_data_service.create_pilot(pilot_data_description=self.pilot_data_description_aws)
        
        #-------------------------------------------------------------------------------------------------#
        # Put all input files, that will be upload  to S3 
        self.input_files = []
        for files in self._cfg_data['input_file_path']:
            self.input_files.append(os.path.join(os.getcwd(), str(files)))
        data_unit_description = { "file_urls": self.input_files }
        print self.input_files
        # Load input data into PD(PD for AWS is S3) 
        self.input_data_unit = self.pd.submit_data_unit(data_unit_description)
        self.input_data_unit.wait()
        logger.info("Input Data Unit URL: " + self.input_data_unit.get_url())
        print "\nData Unit URL: " + self.input_data_unit.get_url()
#        print "\nInput Files:\n ", self.input_data_unit.list()
#        sys.exit (0)
        #-------------------------------------------------------------------------------------------------#
        #get the gene names into a variable
        #self.genes = self._cfg_data['dataset']
        #-------------------------------------------------------------------------------------------------#
        
        #---------------------------------------------------------------------------------------------------
        # create place holder for output data 
        #output_data_unit_description = { "file_urls": [] }
        #self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
        #self.output_data_unit.wait()
        #---------------------------------------------------------------------------------------------------
        #Location for all output files locally
        self.op_path=os.getcwd()+str(self._cfg_data['output_file_path'])
#        print self.op_path
        if os.path.exists(self.op_path):
            shutil.rmtree(self.op_path)
        os.mkdir(self.op_path,0755)
        genes = [] 
        #for gene in self._cfg_data['dataset']:
         #   genes.append(str(gene))
          #  print genes
        #sys.exit (0)
        #########################################################################################################
        self.pilot_compute_service = PilotComputeService(coordination_url=COORD)
        #########################################################################################################

    ##########################################################################################################
    ##Program - CSBLAST
    ##########################################################################################################
    def pgm_csblast(self):
        print "\ncsblast: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['csblast']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['csblast']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("csblast.txt","a")
        print "\ncsblast: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['csblast']) )
        fo.write("\n*******************************")
        fo.write("\ncsblast: Time to set up VM1: %s" %tot_time1)
        #fo.close()
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i "+dataset+".fasta -d /home/ethread/libs/ethread-lib-latest/blast/chain -D /home/ethread/apps/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /home/ethread/apps/blast-2.2.25/bin -o "+dataset+"-chain.csblast"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_blast.txt","stderr_"+dataset+"_chain_blast.txt",dataset+"-chain.csblast"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_blast.txt",
            "error": "stderr_"+dataset+"_chain_blast.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i "+dataset+".fasta -d /home/ethread/libs/ethread-lib-latest/blast/domain -D /home/ethread/apps/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /home/ethread/apps/blast-2.2.25/bin -o "+dataset+"-domain.csblast"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_blast.txt","stderr_"+dataset+"_domain_blast.txt",dataset+"-domain.csblast"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_blast.txt",
            "error": "stderr_"+dataset+"_domain_blast.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
            # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-csiblast",
            "arguments": [" -i "+dataset+"-chain.csblast -o "+dataset+"-chain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.csblast.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        #iterator=iter(list_of_cd_chains)
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            #task = pilotjob1.submit_compute_unit(iterator.next())
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        #if len(task_set_A) > 0:
                        if len(iterator) > 0:
                            #task = pilotjob1.submit_compute_unit(iterator.next())
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-csiblast",
            "arguments": [" -i "+dataset+"-domain.csblast -o "+dataset+"-domain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.csblast.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        #if len(task_set_A) > 0:
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\ncsblast: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        #self.pd.export_du(,self.op_path)
        logger.info("csblast: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\ncsblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program -pftools 
    ##########################################################################################################
    def pgm_pftools(self):
        print "\npftools: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['pftools']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['pftools']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("pftools.txt","a")
        print "\npftools: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['pftools']) )
        fo.write("\n*******************************")
        fo.write("\npftools: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/pftools/pfscan",
            "arguments": [" -a -x -f "+dataset+".fasta /home/ethread/libs/ethread-lib-latest/pftools/chain.prf > "+dataset+"-chain.pftools"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_exec.txt","stderr_"+dataset+"_chain_exec.txt",dataset+"-chain.pftools"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_exec.txt",
            "error": "stderr_"+dataset+"_chain_exec.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/pftools/pfscan",
            "arguments": [" -a -x -f "+dataset+".fasta /home/ethread/libs/ethread-lib-latest/pftools/domain.prf > "+dataset+"-domain.pftools"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_exec.txt","stderr_"+dataset+"_domain_exec.txt",dataset+"-domain.pftools"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_exec.txt",
            "error": "stderr_"+dataset+"_domain_exec.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-pftools",
            "arguments": [" -i "+dataset+"-chain.pftools -o "+dataset+"-chain.pftools.ali -l /home/ethread/libs/ethread-lib-latest/pftools/chain.map"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.pftools.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-pftools",
            "arguments": [" -i "+dataset+"-domain.pftools -o "+dataset+"-domain.pftools.ali -l /home/ethread/libs/ethread-lib-latest/pftools/domain.map"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.pftools.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\ncsblast: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("pftools: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\npftools: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program - pgenthreader
    ##########################################################################################################
    def pgm_pgen(self):
        print "\npgen: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['pgen']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['pgen']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("pgen.txt","a")
        print "\npgen: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['pgen']) )
        fo.write("\n*******************************")
        fo.write("\npgen: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        list_of_cd_chains=[]
        output_du_chain = [] 
        #----------------------------------------------------------------------------------
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/pgen/pGenThreader-chain.sh",
            "arguments": [" pgt-"+dataset+"-chain "+dataset+".fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_exec.txt","stderr_"+dataset+"_chain_exec.txt","pgt-"+dataset+"-chain*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_exec.txt",
            "error": "stderr_"+dataset+"_chain_exec.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)
            
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/pgen/pGenThreader-domain.sh",
            "arguments": [" pgt-"+dataset+"-domain "+dataset+".fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_exec.txt","stderr_"+dataset+"_domain_exec.txt","pgt-"+dataset+"-domain*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_exec.txt",
            "error": "stderr_"+dataset+"_domain_exec.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-pgenthreader",
            "arguments": [" -s pgt-"+dataset+"-chain.pgen.presults -a pgt-"+dataset+"-chain.pgen.align -o "+dataset+"-chain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.pgenthreader.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-pgenthreader",
            "arguments": [" -s pgt-"+dataset+"-domain.pgen.presults -a pgt-"+dataset+"-domain.pgen.align -o "+dataset+"-domain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.pgenthreader.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\npgen: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("pgen: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\npgen: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program - samt2k
    ##########################################################################################################
    def pgm_samt2k(self):
        print "\nsamt2k: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['samt2k']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['samt2k']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("samt2k.txt","a")
        print "\nsamt2k: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['samt2k']) )
        fo.write("\n*******************************")
        fo.write("\nsamt2k: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        list_of_cd_chains=[]
        output_du_A = [] 
        #----------------------------------------------------------------------------------
        i=0
       
        #'A' task 
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_A.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/target2k",
            "arguments": ["  -seed "+dataset+".fasta -out "+dataset+"-t2k"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_A[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_A.txt",
            "error": "stderr_"+dataset+"_A.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)
            
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of target2k exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted 'A' task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One 'A' task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted 'A' task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in 'A' completed. Start 'B'"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of target2k  exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in target2k: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in target2k: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #'B' task
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_B = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_B.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/w0.5",
            "arguments": [" "+dataset+"-t2k.a2m "+dataset+"-t2k-w0.5.mod"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_A[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_B[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_B.txt",
            "error": "stderr_"+dataset+"_B.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of 'B': %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted 'B' task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One 'B' task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted 'B' task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in 'B' completed. Start 'C'"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of 'B' exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in 'B': %s\n" % tot_time
        fo.write("\nTime to complete all tasks in 'B': %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #'C' task
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_C= [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_C.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" "+dataset+"-t2k -i "+dataset+"-t2k-w0.5.mod -calibrate 1"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_B[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_C[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_c.txt",
            "error": "stderr_"+dataset+"_c.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of 'C': %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted 'C' task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One 'C' task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted 'C' task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in 'C' completed. Start 'D'"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of 'C': %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in 'C': %s\n" % tot_time
        fo.write("\nTime to complete all tasks in 'C': %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #'D' task
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_d = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_d.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "sed",
            "arguments": [" -e s/insert\ /model_file\ /g "+dataset+"-t2k.mlib > "+dataset+"-t2k.fix"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_C[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_d[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_d.txt",
            "error": "stderr_"+dataset+"_d.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of d: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted d task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One d task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted d task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in d completed. start chain"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of d: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in d: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in d: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #mv-1 task
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        #output_du_d = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            #output_data_unit_description = { "file_urls": [] }
            #self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            #self.output_data_unit.wait()
            #output_du_d.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mv",
            "arguments": [" "+dataset+"-t2k.fix "+dataset+"-t2k.mlib"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_d[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_d[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_d.txt",
            "error": "stderr_"+dataset+"_d.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of mv: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted mv task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One mv task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted mv task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in mv completed. start chain"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of mv: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in mv: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in mv: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #chain-1
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_mv = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_mv.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" chain -modellibrary "+dataset+"-t2k.mlib -db /home/ethread/libs/ethread-lib-latest/blast/chain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_d[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_mv[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_ch1.txt",
            "error": "stderr_"+dataset+"_ch1.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of ch1: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted ch1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One ch1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted ch1 task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in ch1 completed. start dom1"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of ch1: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in ch1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in ch1: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #domain-1
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        #output_du_dm1 = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            #output_data_unit_description = { "file_urls": [] }
            #self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            #self.output_data_unit.wait()
            #output_du_dm1.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" domain -modellibrary "+dataset+"-t2k.mlib -db /home/ethread/libs/ethread-lib-latest/blast/domain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_d[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_mv[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_dm1.txt",
            "error": "stderr_"+dataset+"_dm1.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of dm1: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted dm1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One dm1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted dm1 task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in dm1 completed. start ch2"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of dm1: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in dm1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in dm1: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #move files
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mv",
            "arguments": [" chain.1."+dataset+"-t2k-w0.5.mod.a2m "+dataset+"-chain-t2k-w0.5.a2m | mv chain.1."+dataset+"-t2k-w0.5.mod.dist "+dataset+"-chain-t2k-w0.5.dist | mv domain.1."+dataset+"-t2k-w0.5.mod.a2m "+dataset+"-domain-t2k-w0.5.a2m | mv domain.1."+dataset+"-t2k-w0.5.mod.dist "+dataset+"-domain-t2k-w0.5.dist"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_mv[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_dm1.txt",
            "error": "stderr_"+dataset+"_dm1.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of dm1: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted dm1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One dm1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted dm1 task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in dm1 completed. start ch2"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of dm1: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in dm1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in dm1: %s" % tot_time)
        ##########################################################----------------------------------------------------------------
        #chain-2
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_ch2= [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_ch2.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" "+dataset+"-chain-t2k-w0.5.a2m -l 10000 > "+dataset+"-chain-t2k-w0.5.aln"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_ch2[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_ch2.txt",
            "error": "stderr_"+dataset+"_ch2.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of ch2: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted ch2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One ch2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted ch2 task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in ch2 completed. start dm2"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of ch2: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in ch2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in ch2: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #domain-2
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_dm2= [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dm2.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" "+dataset+"-domain-t2k-w0.5.a2m -l 10000 > "+dataset+"-domain-t2k-w0.5.aln"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dm2[i].get_url():
                                ["std*",dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_dm2.txt",
            "error": "stderr_"+dataset+"_dm2.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of dm2: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted dm2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One dm2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted dm2 task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in dm2 completed. start chain format"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of dm2: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in dm2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in dm2: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #chain format
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_ch3= [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_ch3.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t "+dataset+".fasta -s "+dataset+"-chain-t2k-w0.5.dist -a "+dataset+"-chain-t2k-w0.5.aln -o "+dataset+"-chain.samt2k.ali"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_ch2[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_ch3[i].get_url():
                                ["std*",dataset+"*.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_ch_format.txt",
            "error": "stderr_"+dataset+"_ch_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. start domain format"
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #domain format
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du_dm3= [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dm3.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t "+dataset+".fasta -s "+dataset+"-domain-t2k-w0.5.dist -a "+dataset+"-domain-t2k-w0.5.aln -o "+dataset+"-domain.samt2k.ali"],
            "environment": ["BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "number_of_processes": 1,
            "input_data": [output_du_dm2[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dm3[i].get_url():
                                ["std*",dataset+"*.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_dom_format.txt",
            "error": "stderr_"+dataset+"_dom_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
            #---------------------------------------------------------------------------------------------------
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)

        print "\nsamt2k: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du_ch3[i],self.op_path)
            self.pd.export_du(output_du_dm3[i],self.op_path)
        logger.info("samt2k: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nsamt2k: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program -psipred
    ##########################################################################################################
    def pgm_psipred(self):
        print "\npsipred: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['psipred']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['psipred']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("psipred.txt","a")
        print "\npsipred: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['psipred']) )
        fo.write("\n*******************************")
        fo.write("\npsipred: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        list_of_cd_chains=[]
        output_du_chain = [] 
        #----------------------------------------------------------------------------------
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/psipred321/runpsipred",
            "arguments": [" "+dataset+".fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["*"+dataset+"*"]
                        }
                 ],
            "output": "stdout_"+dataset+"_psipred.txt",
            "error": "stderr_"+dataset+"_psipred.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)
            
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in psipred completed."
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks: %s\n" % tot_time
        fo.write("\nTime to complete all tasks: %s" % tot_time)
        
        print "\npsipred: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du_chain[i],self.op_path)
        logger.info("psipred: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\npsipred: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program -BLAST
    ##########################################################################################################
    def pgm_blast(self):
        print "\nblast: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['blast']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['blast']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("blast.txt","a")
        print "\nblast: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['blast']) )
        fo.write("\n*******************************")
        fo.write("\nblast: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        list_of_cd_chains=[]
        output_du_chain = [] 
        #----------------------------------------------------------------------------------
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/blast-2.2.25/bin/blastall",
            "arguments": [" -a 2 -p blastp -d /home/ethread/libs/ethread-lib-latest/libs/ncbi/nr -i "+dataset+".fasta -e 400 -I -v 20000 -b 0 -o "+dataset+".blastall"],
            "number_of_processes": 2,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["std*",dataset+".blastall"]
                        }
                 ],
            "output": "stdout_"+dataset+"_psipred.txt",
            "error": "stderr_"+dataset+"_psipred.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)
            
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of blast: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS/2):
            # Submit task to PilotJob
            if len(iterator) > 0:
                task = pilotjob1.submit_compute_unit(iterator.popleft())
                print "* Submitted task '%s' with id '%s'" % (i, task.get_id())
                task_set_A.append(task)
        #---------------------------------------------------------------------------------- 
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in blast completed."
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
        #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks: %s\n" % tot_time
        fo.write("\nTime to complete all tasks: %s" % tot_time)
        
        print "\nblast: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du_chain[i],self.op_path)
        logger.info("blast: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program -hmmer
    ##########################################################################################################
    def pgm_hmmer(self):
        print "\nhmmer: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['hmmer']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['hmmer']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("hmmer.txt","a")
        print "\nhmmer: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['hmmer']) )
        fo.write("\n*******************************")
        fo.write("\nhmmer: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/hmmer-3.0-linux-intel-x86_64/src/hmmscan",
            "arguments": [" --cpu 1 -o "+dataset+"-chain.hmmer --notextw -E 100.0 --max /home/ethread/libs/ethread-lib-latest/hmmer/chain "+dataset+".fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_exec.txt","stderr_"+dataset+"_chain_exec.txt",dataset+"-chain.hmmer"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_exec.txt",
            "error": "stderr_"+dataset+"_chain_exec.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/hmmer-3.0-linux-intel-x86_64/src/hmmscan",
            "arguments": [" --cpu 1 -o "+dataset+"-domain.hmmer --notextw -E 100.0 --max /home/ethread/libs/ethread-lib-latest/hmmer/domain "+dataset+".fasta"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_exec.txt","stderr_"+dataset+"_domain_exec.txt",dataset+"-domain.hmmer"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_exec.txt",
            "error": "stderr_"+dataset+"_domain_exec.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-hmmer",
            "arguments": [" -i "+dataset+"-chain.hmmer -o "+dataset+"-chain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.hmmer.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-hmmer",
            "arguments": [" -i "+dataset+"-domain.hmmer -o "+dataset+"-domain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.hmmer.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nhmmer: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("hmmer: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nhmmer: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ########################################################################################################

    ##########################################################################################################
    ##Program - COMPASS
    ##########################################################################################################
    def pgm_compass(self):
        print "\ncompass: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['compass']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['compass']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("compass.txt","a")
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['compass']) )
        print "\ncompass: Time to set up VM1: ", tot_time1
        fo.write("\n*******************************")
        fo.write("\ncompass: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        output_du_pre1=[] 
        list_of_cd_pre1=[]
       
        i=0
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre1.append(self.output_data_unit)
           #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre1 = {
           "working_directory": str(self._cfg_data['cud_common']['working_directory']),
           "executable": "/home/ethread/apps/blast-2.2.25/bin/blastpgp",
           "arguments": [" -i "+dataset+".fasta -d /home/ethread/libs/ethread-lib-latest/libs/uniprot/uniref90 -h 0.001 -j 5 -m 6 -o "+dataset+".blastout"],
           "number_of_processes": 1,
           "input_data": [self.input_data_unit.get_url()],
           # Put files stdout.txt and stderr.txt into output data unit
           "output_data": [
                       {
                               output_du_pre1[i].get_url():
                               ["stdout_"+dataset+"_chain_pre1.txt","stderr_"+dataset+"_chain_pre1.txt",dataset+".blastout"]
                       }
                ],
           "output": "stdout_"+dataset+"_chain_pre1.txt",
           "error": "stderr_"+dataset+"_chain_pre1.txt"
           }
            i=i+1
            list_of_cd_pre1.append(compute_unit_descriptionpre1)       
           
       #----------------------------------------------------------------------------------
       #list of all the pre-processing 1 compute units that need to be executed
       #---------------------------------------------------------------------------------- 
        iteratorpre1=deque(list_of_cd_pre1)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 1 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
            print "* Submitted pre-processing 1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
               # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
               # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
               # output file.
       
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre1)
                        if len(iteratorpre1) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
                            print "* Submitted pre-processing 1 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 1 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 1 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 1: %s" % tot_time)
       
        #----------------------------------------------------------------------------------
        #Pre processing step 1 completed
        #----------------------------------------------------------------------------------
        #----------------------------------------------------------------------------------
        #Pre processing step 2 
        #---------------------------------------------------------------------------------- 
        list_of_cd_pre2=[]
        output_du_pre2 = [] 
        i=0
       
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre2.append(self.output_data_unit)
           #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre2 = {
           "working_directory": str(self._cfg_data['cud_common']['working_directory']),
           "executable": "/home/ethread/apps/compass-3.1/get_last_br_iter_1.pl",
           "arguments": [" -i "+dataset+".blastout -o "+dataset+".aln1 -c /home/ethread/apps/compass-3.1/prep_psiblastali"],
           "number_of_processes": 1,
           "input_data": [output_du_pre1[i].get_url()],
           # Put files stdout.txt and stderr.txt into output data unit
           "output_data": [
                       {
                               output_du_pre2[i].get_url():
                               ["stdout_"+dataset+"_chain_pre2.txt","stderr_"+dataset+"_chain_pre2.txt",dataset+".aln1"]
                       }
                ],
           "output": "stdout_"+dataset+"_chain_pre2.txt",
           "error": "stderr_"+dataset+"_chain_pre2.txt"
           }
            i=i+1
            list_of_cd_pre2.append(compute_unit_descriptionpre2)       
           
       #----------------------------------------------------------------------------------
       #list of all the pre-processing 2 compute units that need to be executed
       #---------------------------------------------------------------------------------- 
        iteratorpre2=deque(list_of_cd_pre2)
        task_set_preB = list()
        start = time.time()
        print "Start time of pre-processing 2 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
           
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
            print "* Submitted pre-processing 2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preB.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
       
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preB) > 0:
                for a_task in task_set_preB:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preB.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre2)
                        if len(iteratorpre2) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
                            print "* Submitted pre-processing 2 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preB.append(task)
                        else:
                            print "\n All jobs in pre-processing 2 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 2 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 2: %s" % tot_time)
       
        #----------------------------------------------------------------------------------
        #Pre processing step 2 completed
        #----------------------------------------------------------------------------------
        #----------------------------------------------------------------------------------
        #Pre processing step 3 start
        #---------------------------------------------------------------------------------- 
        list_of_cd_pre3=[]
        output_du_pre3 = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre3.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre3 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-fix1",
            "arguments": [" "+dataset+".aln1 "+dataset+".aln"],
            "number_of_processes": 1,
            "input_data": [output_du_pre2[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_pre3[i].get_url():
                                ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+".aln"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_comapss.txt",
            "error": "stderr_"+dataset+"_domain_comapss.txt"
            }
            i=i+1
            list_of_cd_pre3.append(compute_unit_descriptionpre3)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre3=deque(list_of_cd_pre3)
        task_set_pre3 = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_pre3.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_pre3) > 0:
                for a_task in task_set_pre3:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_pre3.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre3)
                        if len(iteratorpre3) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_pre3.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)
       
        #----------------------------------------------------------------------------------
        #Pre processing step 3 completed
        #----------------------------------------------------------------------------------
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i "+dataset+".aln -d /home/ethread/libs/ethread-lib-latest/compass/chain.db -o "+dataset+"-chain.compass"],
            "number_of_processes": 1,
            "input_data": [output_du_pre3[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_compass.txt","stderr_"+dataset+"_chain_compass.txt",dataset+"-chain.compass"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_compass.txt",
            "error": "stderr_"+dataset+"_chain_compass.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i "+dataset+".aln -d /home/ethread/libs/ethread-lib-latest/compass/domain.db -o "+dataset+"-domain.compass"],
            "number_of_processes": 1,
            "input_data": [output_du_pre3[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"-domain.compass"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_comapss.txt",
            "error": "stderr_"+dataset+"_domain_comapss.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-compass",
            "arguments": [" -i "+dataset+"-chain.compass -o "+dataset+"-chain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.compass.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        #if len(task_set_A) > 0:
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd_formatting=[]
        output_du2_dom_format = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-compass",
            "arguments": [" -i "+dataset+"-domain.compass -o "+dataset+"-domain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom_format[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.csblast.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd_formatting.append(compute_unit_descriptiond2)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_formatting)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\ncompass: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        #self.pd.export_du(,self.op_path)
        logger.info("compass: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\ncompass: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program - Threader 
    ##########################################################################################################
    def pgm_threader(self):
        print "\nThreader: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['threader']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['threader']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("threader.txt","a")
        print "\nthreader: Time to set up VM1: ", tot_time1
        fo.write("\n*******************************")
        fo.write("\nthreader: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/threader/threader-linux",
            "arguments": [" -d 200 -p -j "+dataset+".horiz "+dataset+"-chain.threader.out /home/ethread/libs/ethread-lib-latest/threader/chain.lst > "+dataset+"-chain.threader"],
            "number_of_processes": 1,
            "environment": ["export THREAD_DIR=/home/ethread/apps/threader","export TDB_DIR=/home/ethread/libs/ethread-lib-latest/threader/chain"],
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_threader.txt","stderr_"+dataset+"_chain_threader.txt",dataset+"-chain.threader",dataset+"-chain.threader.out"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_threader.txt",
            "error": "stderr_"+dataset+"_chain_threader.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/threader/threader-linux",
            "arguments": [" -d 200 -p -j "+dataset+".horiz "+dataset+"-domain.threader.out /home/ethread/libs/ethread-lib-latest/threader/domain.lst > "+dataset+"-domain.threader"],
            "number_of_processes": 1,
            "environment": ["export THREAD_DIR=/home/ethread/apps/threader","export TDB_DIR=/home/ethread/libs/ethread-lib-latest/threader/domain"],
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_threader.txt","stderr_"+dataset+"_domain_threader.txt",dataset+"-domain.threader",dataset+"-domain.threader.out"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_threader.txt",
            "error": "stderr_"+dataset+"_domain_threader.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
            
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-threader",
            "arguments": [" -s "+dataset+"-chain.threader.out -a "+dataset+"-chain.threader -o "+dataset+"-chain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.threader.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-threader",
            "arguments": [" -s "+dataset+"-domain.threader.out -a "+dataset+"-domain.threader -o "+dataset+"-domain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.threader.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nthreader: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("threader: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nthreader: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program - HHpred 
    ##########################################################################################################
    def pgm_hhpred(self):
        print "\nHHpred: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['hhpred']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['hhpred']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("hhpred.txt","a")
        print "\nhhpred: Time to set up VM1: ", tot_time1
        fo.write("\n*******************************")
        fo.write("\nhhpred: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #Pre processing step 1 
        #----------------------------------------------------------------------------------
        
        output_du_pre1=[] 
        list_of_cd_pre1=[]
        i=0
       
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre1.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/hh_1.5.0.linux64/buildali.pl -fas ",
            "arguments": [" "+dataset+".fasta"],
            "number_of_processes": 1,
            "environment": ["export Data="+"/home/ethread/apps/blast-2.2.25/data","export PERLLIB=$PELLIB:/home/ethread/apps/hh_1.5.0.linux64"],
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [{output_du_pre1[i].get_url():
                                ["stdout_"+dataset+"_chain_pre1.txt","stderr_"+dataset+"_chain_pre1.txt",dataset+".a3m"]}],
            "output": "stdout_"+dataset+"_chain_pre1.txt",
            "error": "stderr_"+dataset+"_chain_pre1.txt"
            }
            i=i+1
            list_of_cd_pre1.append(compute_unit_descriptionpre1)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 1 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre1=deque(list_of_cd_pre1)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 1 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
            print "* Submitted pre-processing 1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
       
        try:
            
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre1)
                        if len(iteratorpre1) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
                            print "* Submitted pre-processing 1 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 1 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 1 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 1: %s" % tot_time)
       
         #----------------------------------------------------------------------------------
         #Pre processing step 1 completed
         #----------------------------------------------------------------------------------
        #----------------------------------------------------------------------------------
        #Pre processing step 2
        #----------------------------------------------------------------------------------
        output_du_pre2=[] 
        list_of_cd_pre2=[]
        i=0
       
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre2.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/hh_1.5.1.1.linux32/hhmake -i ",
            "arguments": [" "+dataset+".a3m"],
            "number_of_processes": 1,
            "environment": ["export BLA_FILE=/home/ethread/apps/"+dataset+".blastall"],
            "input_data": [output_du_pre1[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [{output_du_pre2[i].get_url():
                                ["stdout_"+dataset+"_chain_pre2.txt","stderr_"+dataset+"_chain_pre2.txt",dataset+".a3m"]}],
            "output": "stdout_"+dataset+"_chain_pre2.txt",
            "error": "stderr_"+dataset+"_chain_pre2.txt"
            }
            i=i+1
            list_of_cd_pre2.append(compute_unit_descriptionpre2)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 1 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre2=deque(list_of_cd_pre2)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 2 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
             # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
            print "* Submitted pre-processing 2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        try:
            
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre2)
                        if len(iteratorpre2) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
                            print "* Submitted pre-processing 2 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 2 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 2 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 2: %s" % tot_time)
       
         #----------------------------------------------------------------------------------
         #Pre processing step 2 completed
         #---------------------------------------------------------------------------------- 
        #----------------------------------------------------------------------------------
        #Pre processing step 3
        #----------------------------------------------------------------------------------
        output_du_pre3=[] 
        list_of_cd_pre3=[]
        i=0
       
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre3.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre3 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/hh_1.5.1.1.linux32/hhsearch -cal -i ",
            "arguments": [" "+dataset+".hhm -d /home/ethread/libs/ethread-lib-latest/libs/hhpred/cal.hhm"],
            "number_of_processes": 1,
            "input_data": [output_du_pre2[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [{output_du_pre3[i].get_url():
                                ["stdout_"+dataset+"_chain_pre3.txt","stderr_"+dataset+"_chain_pre3.txt",dataset+".hhr",dataset+".hhm"]}],
            "output": "stdout_"+dataset+"_chain_pre3.txt",
            "error": "stderr_"+dataset+"_chain_pre3.txt"
            }
            i=i+1
            list_of_cd_pre3.append(compute_unit_descriptionpre3)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 3 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre3=deque(list_of_cd_pre3)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 3 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
             # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
            print "* Submitted pre-processing 3 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
       
        try:
            
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 3 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre3)
                        if len(iteratorpre3) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
                            print "* Submitted pre-processing 3 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 3 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 3 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 3: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 3: %s" % tot_time)
       
         #----------------------------------------------------------------------------------
         #Pre processing step 3 completed
         #---------------------------------------------------------------------------------- 
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": "/home/ethread/apps/sp3-chain-"+dataset,
            "executable": "/home/ethread/apps/hh_1.5.1.1.linux32/hhsearch  ",
            "arguments": [" -i "+dataset+".hhm -d /home/ethread/libs/ethread-lib-latest/hhpred/chain.hhm -o "+dataset+"-chain.hhpred -p 1.0"],
            "number_of_processes": 1,
            "environment": ["export Data="+'"/home/ethread/apps/blast-2.2.25/data"'],
            "input_data": [output_du_pre3[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_blast.txt","stderr_"+dataset+"_chain_blast.txt",dataset+"-chain.hhpred"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_blast.txt",
            "error": "stderr_"+dataset+"_chain_blast.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": "/home/ethread/apps",
            "executable": "/home/ethread/apps/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -i "+dataset+".hhm -d /home/ethread/libs/ethread-lib-latest/hhpred/domain.hhm -o "+dataset+"-domain.hhpred -p 1.0"],
            "environment": ["export Data="+'"/home/ethread/apps/blast-2.2.25/data"'],
            "number_of_processes": 1,
            "input_data": [output_du_pre3.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"-domain.hhpred"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_comapss.txt",
            "error": "stderr_"+dataset+"_domain_comapss.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)

        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-hhpred ",
            "arguments": [" -i "+dataset+"-chain.hhpred -o "+dataset+"-chain.hhpred.ali"],
           
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.hhpred.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
        	    # Submit task to PilotJob
            #task = pilotjob1.submit_compute_unit(iterator.next())
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-hhpred ",
            "arguments": [" -i "+dataset+"-domain.hhpred -o "+dataset+"-domain.hhpred.ali"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.hhpred.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        
        pilotjob1.wait()
        self.output_data_unit.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nhhpred: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("HHpred: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nHHpred: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program -SPARKS 
    ##########################################################################################################
    def pgm_sparks(self):
        print "\nSparks: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['sparks']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['sparks']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("csblast.txt","a")
        print "\ncsblast: Time to set up VM1: ", tot_time1
        fo.write("\n*******************************")
        fo.write("\ncsblast: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #Pre processing step 1 
        #----------------------------------------------------------------------------------
        output_du_pre1=[] 
        list_of_cd_pre1=[]
        i=0
       
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre1.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mkdir ",
            "arguments": [" /home/ethread/apps/sparks-chain-"+dataset+" /home/ethread/apps/sparks-domain-"+dataset],
            "number_of_processes": 1,
            # Put files stdout.txt and stderr.txt into output data unit
            "output": "stdout_"+dataset+"_chain_pre1.txt",
            "error": "stderr_"+dataset+"_chain_pre1.txt"
            }
            i=i+1
            list_of_cd_pre1.append(compute_unit_descriptionpre1)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 1 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre1=deque(list_of_cd_pre1)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 1 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
             # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
            print "* Submitted pre-processing 1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
       
        try:
            
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre1)
                        if len(iteratorpre1) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
                            print "* Submitted pre-processing 1 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 1 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 1 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 1: %s" % tot_time)
         #----------------------------------------------------------------------------------
         #Pre processing step 1 completed
         #----------------------------------------------------------------------------------
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": "/home/ethread/apps/sparks-chain-"+dataset,
            "executable": "/home/ethread/apps/sparks/bin/scan_sparks2-chain.job",
            "arguments": [" "+dataset+".fasta"],
            "number_of_processes": 1,
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_blast.txt","stderr_"+dataset+"_chain_blast.txt",dataset+"_spk2.out",dataset+"_spk2_align.dat",dataset+".prf"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_blast.txt",
            "error": "stderr_"+dataset+"_chain_blast.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": "/home/ethread/apps/sparks-domain-"+dataset,
            "executable": "/home/ethread/apps/sparks/bin/scan_sparks2-domain.job",
            "arguments": [" "+dataset+".fasta"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"_spk2.out",dataset+"_spk2_align.dat"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_comapss.txt",
            "error": "stderr_"+dataset+"_domain_comapss.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)
        
        #----------------------------------------------------------------------------------
        #moving chain spk2_align.dat
        #---------------------------------------------------------------------------------- 
        list_of_cd_pre2=[]
        output_du_pre2 = [] 
        i=0
       
        for dataset in genes:
             #---------------------------------------------------------------------------------------------------
             # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre2.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mv ",
            "arguments": [" "+dataset+"_spk2_align.dat "+dataset+"-chain.spk2"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_pre2.txt","stderr_"+dataset+"_chain_pre2.txt",dataset+"-chain.spk2"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_pre2.txt",
            "error": "stderr_"+dataset+"_chain_pre2.txt"
            }
            i=i+1
            list_of_cd_pre2.append(compute_unit_descriptionpre2)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 2 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre2=deque(list_of_cd_pre2)
        task_set_preB = list()
        start = time.time()
        print "Start time of pre-processing 2 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
           
         	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
            print "* Submitted pre-processing 2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preB.append(task)
                  
                 # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                 # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                 # output file.
       
        try:
            flag=len(genes)-NUMBER_PROCESSORS
            while len(task_set_preB) > 0:
                for a_task in task_set_preB:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preB.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre2)
                        if len(iteratorpre2) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
                            print "* Submitted pre-processing 2 task with id '%s'" % task.get_id()
                            flag=flag-1
                            task_set_preB.append(task)
                        else:
                            print "\n All jobs in pre-processing 2 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 2 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 2: %s" % tot_time)
       
         #----------------------------------------------------------------------------------
         #moving 1 spk2_align.dat domain
         #---------------------------------------------------------------------------------- 
        list_of_cd_pre3=[]
        output_du_pre3 = [] 
        i=0
        for dataset in genes:
             #---------------------------------------------------------------------------------------------------
             # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre3.append(self.output_data_unit)
             #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre3 = {
             "working_directory": str(self._cfg_data['cud_common']['working_directory']),
             "executable": "mv ",
             "arguments": [" "+dataset+"_spk2_align.dat "+dataset+"-domain.spk2"],
             "number_of_processes": 1,
             "input_data": [output_du_dom[i].get_url()],
             # Put files stdout.txt and stderr.txt into output data unit
             "output_data": [
                         {
                                 output_du_dom[i].get_url():
                                 ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"-domain.spk2"]
                         }
                  ],
             "output": "stdout_"+dataset+"_domain_comapss.txt",
             "error": "stderr_"+dataset+"_domain_comapss.txt"
             }
            i=i+1
            list_of_cd_pre3.append(compute_unit_descriptionpre3)       
            
         #----------------------------------------------------------------------------------
         #list of all the domain compute units that need to be executed
         #---------------------------------------------------------------------------------- 
        iteratorpre3=deque(list_of_cd_pre3)
        task_set_pre3 = list()
        start = time.time()
        print "Start time of Pre processing step 3 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
            print "* Submitted Pre processing step 3 task '%s' with id '%s'" % (i, task.get_id())
            task_set_pre3.append(task)
                   
                 # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                 # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                 # output file.
        
        try:
            while len(task_set_pre3) > 0:
                for a_task in task_set_pre3:
                    if (a_task.get_state() == "Done"):
                        print "One Pre processing step 3 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_pre3.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre3)
                        if len(iteratorpre3) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
                            print "* Submitted Pre processing step 3 task with id '%s'" % task.get_id()
                            task_set_pre3.append(task)
                        else:
                            print "\n All jobs in Pre processing step 3 completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of Pre processing step 3 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in Pre processing step 3: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in Pre processing step 3: %s" % tot_time)
       
        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-sparks",
            "arguments": [" -s "+dataset+"_spk2.out -a "+dataset+"-chain.spk2 -o "+dataset+"-chain.sparks2.ali"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.sparks2.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-sparks",
            "arguments": [" -s "+dataset+"_spk2.out -a "+dataset+"-domain.spk2 -o "+dataset+"-domain.sparks2.ali"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.sparks2.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nSparks: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("Sparks: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nSparks: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program - SP3 
    ##########################################################################################################
    def pgm_sp3(self):
        print "\nSP3: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['sp3']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['sp3']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }
        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("sp3.txt","a")
        print "\nsp3: Time to set up VM1: ", tot_time1
        fo.write("\n*******************************")
        fo.write("\nsp3: Time to set up VM1: %s" %tot_time1)
       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        #----------------------------------------------------------------------------------
        #Pre processing step 1 
        #----------------------------------------------------------------------------------
        output_du_pre1=[] 
        list_of_cd_pre1=[]
        i=0
       
        for dataset in genes:
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre1.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre1 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mkdir ",
            "arguments": [" /home/ethread/apps/sp3-chain-"+dataset+" /home/ethread/apps/sp3-domain-"+dataset],
            "number_of_processes": 1,
            # Put files stdout.txt and stderr.txt into output data unit
            "output": "stdout_"+dataset+"_chain_pre1.txt",
            "error": "stderr_"+dataset+"_chain_pre1.txt"
            }
            i=i+1
            list_of_cd_pre1.append(compute_unit_descriptionpre1)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 1 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre1=deque(list_of_cd_pre1)
        task_set_preA = list()
        start = time.time()
        print "Start time of pre-processing 1 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
             # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
            print "* Submitted pre-processing 1 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preA.append(task)
                  
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
       
        try:
            
            while len(task_set_preA) > 0:
                for a_task in task_set_preA:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 1 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preA.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre1)
                        if len(iteratorpre1) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre1.popleft())
                            print "* Submitted pre-processing 1 task with id '%s'" % task.get_id()
                            task_set_preA.append(task)
                        else:
                            print "\n All jobs in pre-processing 1 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 1 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 1: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 1: %s" % tot_time)
         #----------------------------------------------------------------------------------
         #Pre processing step 1 completed
         #----------------------------------------------------------------------------------
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionc1 = {
            "working_directory": "/home/ethread/apps/sp3-chain-"+dataset,
            "executable": "/home/ethread/apps/sparks/bin/scan_sp3-chain.job",
            "arguments": [" "+dataset+".fasta"],
            "number_of_processes": 1,
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_blast.txt","stderr_"+dataset+"_chain_blast.txt",dataset+"_sp3.out",dataset+"_sp3_align.dat"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_blast.txt",
            "error": "stderr_"+dataset+"_chain_blast.txt"
            }
            i=i+1
            list_of_cd_chains.append(compute_unit_descriptionc1)       
            
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_chains)
        task_set_A = list()
        start = time.time()
        print "Start time of chain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
            
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain completed. Start Domain"

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #Domain part jobs            
        ##########################################################----------------------------------------------------------------
        list_of_cd_domain=[]
        output_du_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptiond1 = {
            "working_directory": "/home/ethread/apps/sp3-"+dataset,
            "executable": "/home/ethread/apps/sparks/bin/scan_sp3-domain.job",
            "arguments": [" "+dataset+".fasta"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"_sp3.out",dataset+"_sp3_align.dat"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_comapss.txt",
            "error": "stderr_"+dataset+"_domain_comapss.txt"
            }
            i=i+1
            list_of_cd_domain.append(compute_unit_descriptiond1)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd_domain)
        task_set_A = list()
        start = time.time()
        print "Start time of domain exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain: %s" % tot_time)
        
        #----------------------------------------------------------------------------------
        #moving chain spk2_align.dat
        #---------------------------------------------------------------------------------- 
        list_of_cd_pre2=[]
        output_du_pre2 = [] 
        i=0
       
        for dataset in genes:
             #---------------------------------------------------------------------------------------------------
             # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre2.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre2 = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "mv ",
            "arguments": [" "+dataset+"_sp3_align.dat "+dataset+"-chain.sp3"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_pre2.txt","stderr_"+dataset+"_chain_pre2.txt",dataset+"]-chain.sp3"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_pre2.txt",
            "error": "stderr_"+dataset+"_chain_pre2.txt"
            }
            i=i+1
            list_of_cd_pre2.append(compute_unit_descriptionpre2)       
           
        #----------------------------------------------------------------------------------
        #list of all the pre-processing 2 compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iteratorpre2=deque(list_of_cd_pre2)
        task_set_preB = list()
        start = time.time()
        print "Start time of pre-processing 2 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
            print "* Submitted pre-processing 2 task '%s' with id '%s'" % (i, task.get_id())
            task_set_preB.append(task)
                  
                 # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                 # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                 # output file.
       
        try:
            while len(task_set_preB) > 0:
                for a_task in task_set_preB:
                    if (a_task.get_state() == "Done"):
                        print "One pre-processing 2 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_preB.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre2)
                        if len(iteratorpre2) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre2.popleft())
                            print "* Submitted pre-processing 2 task with id '%s'" % task.get_id()
                            task_set_preB.append(task)
                        else:
                            print "\n All jobs in pre-processing 2 completed. "

        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of pre-processing 2 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in pre-processing 2: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in pre-processing 2: %s" % tot_time)
         #----------------------------------------------------------------------------------
         #moving 1 spk2_align.dat domain
         #---------------------------------------------------------------------------------- 
        list_of_cd_pre3=[]
        output_du_pre3 = [] 
        i=0
        for dataset in genes:
             #---------------------------------------------------------------------------------------------------
             # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du_pre3.append(self.output_data_unit)
             #---------------------------------------------------------------------------------------------------
            compute_unit_descriptionpre3 = {
             "working_directory": str(self._cfg_data['cud_common']['working_directory']),
             "executable": "mv ",
             "arguments": [" "+dataset+"_sp3_align.dat "+dataset+"-domain.sp3"],
             "number_of_processes": 1,
             "input_data": [output_du_dom[i].get_url()],
             # Put files stdout.txt and stderr.txt into output data unit
             "output_data": [
                         {
                                 output_du_dom[i].get_url():
                                 ["stdout_"+dataset+"_domain_comapss.txt","stderr_"+dataset+"_domain_comapss.txt",dataset+"-domain.sp3"]
                         }
                  ],
             "output": "stdout_"+dataset+"_domain_comapss.txt",
             "error": "stderr_"+dataset+"_domain_comapss.txt"
             }
            i=i+1
            list_of_cd_pre3.append(compute_unit_descriptionpre3)       
            
         #----------------------------------------------------------------------------------
         #list of all the domain compute units that need to be executed
         #---------------------------------------------------------------------------------- 
        iteratorpre3=deque(list_of_cd_pre3)
        task_set_pre3 = list()
        start = time.time()
        print "Start time of Pre processing step 3 exec: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
            print "* Submitted Pre processing step 3 task '%s' with id '%s'" % (i, task.get_id())
            task_set_pre3.append(task)
                   
                 # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                 # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                 # output file.
        
        try:
            while len(task_set_pre3) > 0:
                for a_task in task_set_pre3:
                    if (a_task.get_state() == "Done"):
                        print "One Pre processing step 3 task %s finished. Launching next task." % (a_task.get_id())
                        task_set_pre3.remove(a_task)
                         # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iteratorpre3)
                        if len(iteratorpre3) > 0:
                            task = pilotjob1.submit_compute_unit(iteratorpre3.popleft())
                            print "* Submitted Pre processing step 3 task with id '%s'" % task.get_id()
                            task_set_pre3.append(task)
                        else:
                            print "\n All jobs in Pre processing step 3 completed. Start formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                     #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of Pre processing step 3 exec: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in Pre processing step 3: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in Pre processing step 3: %s" % tot_time)
       
        ##########################################################----------------------------------------------------------------
        #Formatting all the chain 
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s "+dataset+"_sp3.out -a "+dataset+"-chain.sp3 -o "+dataset+"-chain.sp3.ali"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [output_du_chain[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_format.txt","stderr_"+dataset+"_chain_format.txt",dataset+"-chain.sp3.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_format.txt",
            "error": "stderr_"+dataset+"_chain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of tasks: '%s'" % len(task_set_A)
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #format domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "/home/ethread/apps/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s "+dataset+"_sp3.out -a "+dataset+"-domain.sp3 -o "+dataset+"-domain.sp3.ali"],
            "environment": ["export sparks=/home/ethread/apps/sparks","export PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [output_du_dom[i].get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_format.txt","stderr_"+dataset+"_domain_format.txt",dataset+"-domain.sp3.ali"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_format.txt",
            "error": "stderr_"+dataset+"_domain_format.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
       	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "\nlen of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nSP3: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("SP3: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nSP3: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called 
    ########################################################################################################
    def go(self):
        threads=[]

        #Thread for csblast
        if not 'csblast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_csblast)
            thread.start()
       
        #Thread for pfTools
        if not 'pftools' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_pftools)
            thread.start()

        #Thread for HMMER
        if not 'hmmer' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_hmmer)
            thread.start()

        #Thread for pGenThreader
        if not 'pgen' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_pgen)
            thread.start()

        #Thread for psipred and threader 
        if not 'threader' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_psipred)
            thread.start()
            thread.join()
            thread=threading.Thread(target=self.pgm_threader)
            thread.start()
        

        #Thread for blast
        if not 'blast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_blast)
            thread.start()

        #Thread for Blast+SAMT2K 
        if not 'samt2k' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_blast)
            thread.start()
            thread.join()
            thread=threading.Thread(target=self.pgm_samt2k)
            thread.start()

        #Thread for compass 
        if not 'blast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_compass)
            thread.start()

        #Thread for SPARKS
        if not 'blast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_sparks)
            thread.start()

        #Thread for SP3
        if not 'blast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_sp3)
            thread.start()

        #Thread for HHpred 
        if not 'blast' in self._cfg_data['program_to_omit'] :
            thread=threading.Thread(target=self.pgm_hhpred)
            thread.start()

            threads.append(thread)

        for thread in threads:
            thread.join()
    #########################################################################################################            
        
    ##########################################################################################################
    ##Program- ethread
    ##########################################################################################################
    def pgm_ethread(self):
        #create Data units
        print "\nethread: Creating data units"
        #############################################################       
        genes = [] 
        for gene in self._cfg_data['dataset']:
            genes.append(str(gene))
        print genes
        #genes=["1a8dA","1acmC","1bfmB","1bg1B"]
        loc = "/home/cctsg/amazon/working/anjani_benchmarks/dataali/"
        #############################################################       
        #cat cmd
        #############################################################       
        for dataset in genes:
            #try cat locally-chain
            os.system("cat "+loc+dataset+"-chain.compass.ali "+loc+dataset+"-chain.csblast.ali "+loc+dataset+"-chain.hhpred.ali "+loc+dataset+"-chain.hmmer.ali "+loc+dataset+"-chain.pftools.ali "+loc+dataset+"-chain.pgenthreader.ali "+loc+dataset+"-chain.samt2k.ali "+loc+dataset+"-chain.sp3.ali "+loc+dataset+"-chain.sparks2.ali "+loc+dataset+"-chain.threader.ali > "+dataset+"-chain.ali")
            self.input_files.append(os.path.join(os.getcwd(),dataset+"-chain.ali"))
            #try cat locally-domain
            os.system("cat "+loc+dataset+"-domain.compass.ali "+loc+dataset+"-domain.csblast.ali "+loc+dataset+"-domain.hhpred.ali "+loc+dataset+"-domain.hmmer.ali "+loc+dataset+"-domain.pftools.ali "+loc+dataset+"-domain.pgenthreader.ali "+loc+dataset+"-domain.samt2k.ali "+loc+dataset+"-domain.sp3.ali "+loc+dataset+"-domain.sparks2.ali "+loc+dataset+"-domain.threader.ali > "+dataset+"-domain.ali")
            self.input_files.append(os.path.join(os.getcwd(),dataset+"-domain.ali"))

            print self.input_files
        #-------------------------------------------------------------------------------------------------#
        # Put all input files, that will be upload  to S3 
        data_unit_description1 = { "file_urls": self.input_files }
        self.input_data_unit = self.pd.submit_data_unit(data_unit_description1)
        self.input_data_unit.wait()

        print "\nethread: Creating VM instance"
        json_parse = self._cfg_data['pcd_common']
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": str(json_parse['service_url']),
                             "number_of_processes": int(json_parse['number_of_processes']),
                             "vm_id": str(self._cfg_data['vm_id']['ethread']),
                             "vm_ssh_username":str(json_parse['vm_ssh_username']),
                             "vm_ssh_keyname":str(json_parse['vm_ssh_keyname']),
                             "vm_ssh_keyfile":str(json_parse['vm_ssh_keyfile']),
                             "vm_type":str(self._cfg_data['vm_type']['ethread']),
                             "region" :str(json_parse['region']),
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = self.pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        fo=open("ethread.txt","a")
        print "\nethread: Time to set up VM1: ", tot_time1
        fo.write("\n*%s*" % str(self._cfg_data['vm_type']['ethread']) )
        fo.write("\n*******************************")
        fo.write("\nethread: Time to set up VM1: %s" %tot_time1)
       
        NUMBER_PROCESSORS=int(json_parse['number_of_processes'])
        
        #----------------------------------------------------------------------------------
        #list of all the chain compute units that need to be executed
        #----------------------------------------------------------------------------------
        list_of_cd_chains=[]
        output_du_chain = [] 
        i=0
        ##########################################################----------------------------------------------------------------
        #ethread - chain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_chain = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_chain.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "cat",
            "arguments": [" "+dataset+"-chain.ali | /home/ethread/apps/ethread-1.0/bin/ethread -t "+dataset+".fasta -l /home/ethread/libs/ethread-lib-latest/blast/chain -o "+dataset+"-chain.ethread -f "+dataset+"-chain.ethread-fun -d chain "],
            "environment": ["ET_THREADMOD=/home/ethread/apps/ethread-1.0/mod","ET_BINLIBSVM=/home/ethread/apps/libsvm-3.11/svm-predict"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_chain[i].get_url():
                                ["stdout_"+dataset+"_chain_ethread.txt","stderr_"+dataset+"_chain_ethread.txt",dataset+"-chain.ethread"]
                        }
                 ],
            "output": "stdout_"+dataset+"_chain_ethread.txt",
            "error": "stderr_"+dataset+"_chain_ethread.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)       
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        #iterator=iter(list_of_cd_chains)
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of chain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted chain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One chain task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted chain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in chain format completed. Start domain formatting"
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of chain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in chain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in chain format: %s" % tot_time)
        
        ##########################################################----------------------------------------------------------------
        #ethread - domain
        ##########################################################----------------------------------------------------------------
        list_of_cd=[]
        output_du2_dom = [] 
        i=0
        for dataset in genes:
            #---------------------------------------------------------------------------------------------------
            # create place holder for output data 
            output_data_unit_description = { "file_urls": [] }
            self.output_data_unit = self.pd.submit_data_unit(output_data_unit_description)
            self.output_data_unit.wait()
            output_du2_dom.append(self.output_data_unit)
            #---------------------------------------------------------------------------------------------------
            compute_unit_description = {
            "working_directory": str(self._cfg_data['cud_common']['working_directory']),
            "executable": "cat",
            "arguments": [" "+dataset+"-domain.ali | /home/ethread/apps/ethread-1.0/bin/ethread -t "+dataset+".fasta -l /home/ethread/libs/ethread-lib-latest/blast/domain -o "+dataset+"-domain.ethread -f "+dataset+"-.ethread-fun -d domain"],
            "environment": ["ET_THREADMOD=/home/ethread/apps/ethread-1.0/mod","ET_BINLIBSVM=/home/ethread/apps/libsvm-3.11/svm-predict"],
            "number_of_processes": 1,
            "input_data": [self.input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_du2_dom[i].get_url():
                                ["stdout_"+dataset+"_domain_ethread.txt","stderr_"+dataset+"_domain_ethread.txt",dataset+"-domain.ethread"]
                        }
                 ],
            "output": "stdout_"+dataset+"_domain_ethread.txt",
            "error": "stderr_"+dataset+"_domain_ethread.txt"
            }
            i=i+1
            list_of_cd.append(compute_unit_description)
            
        #----------------------------------------------------------------------------------
        #list of all the domain compute units that need to be executed
        #---------------------------------------------------------------------------------- 
        iterator=deque(list_of_cd)
        task_set_A = list()
        start = time.time()
        print "Start time of domain format: %s" % time.asctime(time.localtime(start) )
        for i in range(NUMBER_PROCESSORS):
        	    # Submit task to PilotJob
            task = pilotjob1.submit_compute_unit(iterator.popleft())
            print "* Submitted domain format task '%s' with id '%s'" % (i, task.get_id())
            task_set_A.append(task)
                   
                # Chaining tasks i.e submit a compute unit, when compute unit from A is successfully executed.
                # A 'B' task reads the content of the output file of an 'A' task and writes it into its own
                # output file.
        
        try:
            while len(task_set_A) > 0:
                for a_task in task_set_A:
                    if (a_task.get_state() == "Done"):
                        print "One domain format task %s finished. Launching next task." % (a_task.get_id())
                        task_set_A.remove(a_task)
                        # Submit task to Pilot Job
                        print "len of tasks: '%s'" % len(task_set_A)
                        print "len of iterator: '%s'" % len(iterator)
                        if len(iterator) > 0:
                            task = pilotjob1.submit_compute_unit(iterator.popleft())
                            print "* Submitted domain format task with id '%s'" % task.get_id()
                            task_set_A.append(task)
                        else:
                            print "\n All jobs in domain format completed."
                    
        except Exception, ex:
            print "AN ERROR OCCURRED: %s" % ((str(ex)))
                    #------------------------------------------------------------------------------
        pilotjob1.wait()
        end = time.time()
        tot_time = end - start
        print "End time of domain format: %s" % time.asctime(time.localtime(end) )
        print "Time to complete all tasks in domain format: %s\n" % tot_time
        fo.write("\nTime to complete all tasks in domain format: %s" % tot_time)
        
        print "\nethread: exporting du to local"
        for i in range(len(genes)):
            self.pd.export_du(output_du2_chain[i],self.op_path)
            self.pd.export_du(output_du2_dom[i],self.op_path)
        logger.info("ethread: Terminate Pilot Compute/Data Service")
        fo.close()
        print "\nethread: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ############################################################################################################

        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

        #cancel pilot compute/data services
        self.pilot_data_service.cancel() 
        self.pilot_compute_service.cancel()
             
#################################################################################################################################
if __name__ == "__main__":

    #check for config file input 
    if len(sys.argv) != 2:
        sys.exit('Usage: %s <cfg-file>' % sys.argv[0])

    #Object to the class mainThread() with the config file as an arg
    obj = mainThread(sys.argv[1])
    #To call threads
    obj.go()
    #ethread will execute after all threads complete
    # cat all the ali to one file:
    #obj.pgm_ethread()

