####################################################################################################
#BigJob program to submit multiple jobs to multiple VMs in EC2
#Uses multi-threading to submit each program to each VM that will be created simultaneously
#Program-1-csblast
#Program-2-pftools
#Program-3-hmmer
#Program-4-psipred+threader
#Program-5-hhpred
#Program-6-sparks
#Program-7-sp3
#Program-8-compass
#Program-9-samt2k
#Program-10-ethread (works after all the programs complete execution)
####################################################################################################
import threading
import os
import time
import sys
import logging
import uuid

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

# create pilot data service (factory for data pilots (physical, distributed storage))
# and pilot data
###################################################################################################
##Pilot-data Service
###################################################################################################
print "\nSetting up Pilot Data Service in S3"

pilot_data_service = PilotDataService(coordination_url=COORD)
pilot_data_description_aws={
                                "service_url": "s3://pilot-data-" + str(uuid.uuid1()),
                                "size": 100,
                                "access_key_id":ACCESS_KEY_ID,
                                "secret_access_key":SECRET_ACCESS_KEY
                           }

pd = pilot_data_service.create_pilot(pilot_data_description=pilot_data_description_aws)

#-------------------------------------------------------------------------------------------------#
# Put all input files, that will be upload  to S3  
data_unit_description = { "file_urls": [os.path.join(os.getcwd(), "data/F6VMN7.fasta"), os.path.join(os.getcwd(), "data/sparks-chain.sh"), os.path.join(os.getcwd(), "data/sparks-domain.sh"),os.path.join(os.getcwd(), "data/sp3-chain.sh"),os.path.join(os.getcwd(), "data/sp3-domain.sh") ] }
#-------------------------------------------------------------------------------------------------#

# Load input data into PD(PD for AWS is S3) 
input_data_unit = pd.submit_data_unit(data_unit_description)
input_data_unit.wait()
logger.info("Input Data Unit URL: " + input_data_unit.get_url())
print "\nData Unit URL: " + input_data_unit.get_url()

#########################################################################################################
pilot_compute_service = PilotComputeService(coordination_url=COORD)
#########################################################################################################

class mainThread:
    def __init__(self):
        self.running = True

    ##########################################################################################################
    ##Program-1 - CSBLAST
    ##########################################################################################################
    def pgm_csblast(self):
        print "\ncsblast: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\ncsblast: Time to set up VM1: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-1-CS BLAST: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/chain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-chain.csblast"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.csblast"]
                        }
                 ],
            "output": "stdout_chain_blast.txt",
            "error": "stderr_chain_blast.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("csblast-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/domain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-domain.csblast"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.csblast"]
                        }
                 ],
            "output": "stdout_domain_blast.txt",
            "error": "stderr_domain_blast.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-1: input files:\t %s\n" %input_data_unit.list()
        print "csblast: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\ncsblast: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-chain.csblast -o F6VMN7-chain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_blast_final.txt",
            "error": "stderr_chain_blast_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-domain.csblast -o F6VMN7-domain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_blast_final.txt",
            "error": "stderr_domain_blast_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-2: input files:\t %s\n" %input_data_unit.list()
        print "csblast: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\ncsblast: Time for part 2: ", tot_time
        logger.info("csblast: Terminate Pilot Compute/Data Service")
        print "\ncsblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-2 - pftools
    ##########################################################################################################
    def pgm_pftools(self):
        print "\npftools: Creating VM instance"
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        start_time = time.time()
        pilot_compute_description_amazon_west2 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob2 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        print "\npftools: Time to set up VM2: ", tot_time
        #----------------------------------------------------------------------------------
        #Cumpute Unit Description - program pftools part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-2-pf tools: compute unit starts here"
        start_time = time.time()
        # create compute unit-pftools-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/bin/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/chain.prf > F6VMN7-chain.pftools"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_chain_pftools.txt",
            "error": "stderr_chain_pftools.txt"
        }
        compute_unitc1 = pilotjob2.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/bin/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/domain.prf > F6VMN7-domain.pftools"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_domain_pftools.txt",
            "error": "stderr_domain_pftools.txt"
        }
        compute_unitd1 = pilotjob2.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("pftools-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "pftools: part-1: input files:\t %s\n" %input_data_unit.list()
        print "pftools: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\npftools: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - pftools: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-pftools-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-chain.pftools -o F6VMN7-chain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/chain.map"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_pftools_final.txt",
            "error": "stderr_chain_pftools_final.txt"
        }
        compute_unitc2 = pilotjob2.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-domain.pftools -o F6VMN7-domain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/domain.map"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_pftools_final.txt",
            "error": "stderr_domain_pftools_final.txt"
        }

        compute_unitd2 = pilotjob2.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob2.wait()

        tot_time = time.time() - start_time
        print "\npftools: Time for part 2: ", tot_time
        logger.info("pftools: Terminate Pilot Compute/Data Service")
        print "\npftools: Terminate Pilot Compute/Data Service"
        pilotjob2.cancel()
       ################################################################################################################

    ##########################################################################################################
    ##Program-3 - hmmer
    ##########################################################################################################
    def pgm_hmmer(self):
        print "\nhmmer: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob3 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nhmmer: Time to set up VM3: ", tot_time1
        #-------------------------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program hmmer part-1
        #-------------------------------------------------------------------------------------------------
        print "\nhmmer: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-hmmer-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-chain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_chain_hmmer.txt",
            "error": "stderr_chain_hmmer.txt"
        }
        compute_unitc1 = pilotjob3.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hmmer-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-domain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_domain_hmmer.txt",
            "error": "stderr_domain_hmmer.txt"
        }
        compute_unitd1 = pilotjob3.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob3.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-1: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - hmmer: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-hmmer-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-chain.hmmer -o F6VMN7-chain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_hmmer_final.txt",
            "error": "stderr_chain_hmmer_final.txt"
        }
        compute_unitc2 = pilotjob3.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hmmer-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-domain.hmmer -o F6VMN7-domain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_hmmer_final.txt",
            "error": "stderr_domain_hmmer_final.txt"
        }

        compute_unitd2 = pilotjob3.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob3.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-2: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 2: ", tot_time
        logger.info("hmmer: Terminate Pilot Compute/Data Service")
        print "\nhmmer: Terminate Pilot Compute/Data Service"
        pilotjob3.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-4 - psipred and Threader
    ##########################################################################################################
    def pgm_psipred_threader(self):
        print "\npsipred: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob4 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\npsipred: Time to set up VM4: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program psipred part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-4-psipred: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/psipred321/runpsipred",
            "arguments": [" F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_psipred.txt",
            "error": "stderr_psipred.txt"
        }
        compute_unit = pilotjob4.submit_compute_unit(compute_unit_description)
        logger.info("psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "psipred: input files:\t %s\n" %input_data_unit.list()
        print "psipred: output files:\t %s\n" %output_data_unit.list()
        print "\npsipred: Time for psipred: ", tot_time
        #********************************************************************************************#
        # Start Threader after psipred                                                               #
        # Compute Unit Description12 - threader: Part-2                                              #
        #********************************************************************************************#
        print "\nProgram-4-Threader part: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-chain.threader.out /usr/local/libraries/ethread-lib-latest/threader/chain.lst > F6VMN7-chain.threader"],
            "environment": ["THREAD_DIR=/usr/local/threader","TDB_DIR=/usr/local/libraries/ethread-lib-latest/threader/chain"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_chain_threader.txt",
            "error": "stderr_chain_threader.txt"
        }
        compute_unitc1 = pilotjob4.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-domain.threader.out /usr/local/libraries/ethread-lib-latest/threader/domain.lst > F6VMN7-domain.threader"],
            "environment": ["THREAD_DIR=/usr/local/threader","TDB_DIR=/usr/local/libraries/ethread-lib-latest/threader/domain"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_domain_threader.txt",
            "error": "stderr_domain_threader.txt"
        }
        compute_unitd1 = pilotjob4.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-1: input files:\t %s\n" %input_data_unit.list()
        print "threader: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nthreader: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-chain.threader.out -a F6VMN7-chain.threader -o F6VMN7-chain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_chain_threader_final.txt",
            "error": "stderr_chain_threader_final.txt"
        }
        compute_unitc2 = pilotjob4.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-domain.threader.out -a F6VMN7-domain.threader -o F6VMN7-domain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_domain_threader_final.txt",
            "error": "stderr_domain_threader_final.txt"
        }

        compute_unitd2 = pilotjob4.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-2: input files:\t %s\n" %input_data_unit.list()
        print "threader: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nthreader: Time for part 2: ", tot_time
        logger.info("psipred-threader: Terminate Pilot Compute/Data Service")
        print "\npsipred-threader: Terminate Pilot Compute/Data Service"
        pilotjob4.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-5 - hhpred 
    ##########################################################################################################
    def pgm_hhpred(self):
        print "\nhhpred: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.small",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob5 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nhhpred: Time to set up VM5: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program HHPRED part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-hhpred: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-for perl part
        compute_unit_description1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "perl",
            "arguments": ["/usr/local/hh_1.5.0.linux64/buildali.pl -fas F6VMN7.fasta"],
            "environment": ["Data=/usr/local/blast-2.2.25/data","PERL5LIB=$PERL5LIB:/usr/local/hh_1.5.0.linux64"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7.*"] } ],
            "output": "stdout_perl_hhpred.txt",
            "error": "stderr_perl_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        logger.info("hhpred-perl-part: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-perl-part: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unit1.wait()

        # create compute unit-hhm-part1
        compute_unit_description1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hh_1.5.1.1.linux32/hhmake",
            "arguments": [" -i F6VMN7.a3m"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7.*"] } ],
            "output": "stdout_hhm1_hhpred.txt",
            "error": "stderr_hhm1_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        compute_unit1.wait()

        # create compute unit-hhm-part2
        compute_unit_description1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -cal -i F6VMN7.hhm -d /usr/local/libraries/ethread-lib-latest/libs/hhpred/cal.hhm"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7.*"] } ],
            "output": "stdout_hhm2_hhpred.txt",
            "error": "stderr_hhm2_hhpred.txt"
        }
        compute_unit1 = pilotjob5.submit_compute_unit(compute_unit_description1)
        logger.info("hhpred-hhm-part: Finished setup of Computeunit. Waiting for scheduling of PD")
        print "hhpred-hhm-part: Finished setup of Computeunit. Waiting for scheduling of PD"
        compute_unit1.wait()

        #**************************************************************************
        #HHPRED CHAIN & DOMAIN PART 1                                             *
        #**************************************************************************
        # create compute unit-hhpred-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -i F6VMN7.hhm -d /usr/local/libraries/ethread-lib-latest/hhpred/chain.hhm -o F6VMN7-chain.hhpred -p 1.0"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*hhpred*"]
                        }
                 ],
            "output": "stdout_chain_hhpred.txt",
            "error": "stderr_chain_hhpred.txt"
        }
        compute_unitc1 = pilotjob5.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("hhpred-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hhpred-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hh_1.5.1.1.linux32/hhsearch",
            "arguments": [" -i F6VMN7.hhm -d /usr/local/libraries/ethread-lib-latest/hhpred/domain.hhm -o F6VMN7-domain.hhpred -p 1.0"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*hhpred*"]
                        }
                 ],
            "output": "stdout_domain_hhpred.txt",
            "error": "stderr_domain_hhpred.txt"
        }
        compute_unitd1 = pilotjob5.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("hhpred-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob5.wait()
        tot_time = time.time() - start_time
        print "hhpred: part-1: input files:\t %s\n" %input_data_unit.list()
        print "hhpred: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nhhpred: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - hhpred: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hhpred",
            "arguments": [" -i F6VMN7-chain.hhpred -o F6VMN7-chain.hhpred.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_hhpred_final.txt",
            "error": "stderr_chain_hhpred_final.txt"
        }
        compute_unitc2 = pilotjob5.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("hhpred-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hhpred",
            "arguments": ["-i F6VMN7-domain.hhpred -o F6VMN7-domain.hhpred.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_hhpred_final.txt",
            "error": "stderr_domain_hhpred_final.txt"
        }

        compute_unitd2 = pilotjob5.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("hhpred: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hhpred: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob5.wait()
        tot_time = time.time() - start_time
        print "hhpred: part-2: input files:\t %s\n" %input_data_unit.list()
        print "hhpred: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nhhpred: Time for part 2: ", tot_time
        logger.info("hhpred: Terminate Pilot Compute/Data Service")
        print "\nhhpred: Terminate Pilot Compute/Data Service"
        pilotjob5.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-6 - sparks
    ##########################################################################################################
    def pgm_sparks(self):
        print "\nsparks: Creating VM instance"
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        start_time = time.time()
        pilot_compute_description_amazon_west2 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.small",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob6 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        print "\nsparks: Time to set up VM: ", tot_time
        #----------------------------------------------------------------------------------
        #Cumpute Unit Description - program sparks part-1
        #----------------------------------------------------------------------------------
        print "\nsparks: compute unit starts here"
        start_time = time.time()
        # create compute unit-sparks-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "sh",
            "arguments": [" sparks-chain.sh"],
            "environment": ["sparks=/usr/local/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_sparks.txt",
            "error": "stderr_chain_sparks.txt"
        }
        compute_unitc1 = pilotjob6.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("sparks-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sparks-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "sh",
            "arguments": [" sparks-domain.sh"],
            "environment": ["sparks=/usr/local/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_sparks.txt",
            "error": "stderr_domain_sparks.txt"
        }
        compute_unitd1 = pilotjob6.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("sparks-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob6.wait()
        tot_time = time.time() - start_time
        print "sparks: part-1: input files:\t %s\n" %input_data_unit.list()
        print "sparks: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nsparks: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - sparks: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-sparks-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-sparks",
            "arguments": ["-s F6VMN7-chain.spk2.out -a F6VMN7-chain.spk2 -o F6VMN7-chain.sparks2.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_sparks_final.txt",
            "error": "stderr_chain_sparks_final.txt"
        }
        compute_unitc2 = pilotjob6.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("sparks-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sparks-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-sparks",
            "arguments": ["-s F6VMN7-domain.spk2.out -a F6VMN7-domain.spk2 -o F6VMN7-domain.sparks2.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_sparks_final.txt",
            "error": "stderr_domain_sparks_final.txt"
        }

        compute_unitd2 = pilotjob6.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("sparks-domain: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sparks-domain: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob6.wait()

        tot_time = time.time() - start_time
        print "\nsparks: Time for part 2: ", tot_time
        logger.info("sparks: Terminate Pilot Compute/Data Service")
        print "\nsparks: Terminate Pilot Compute/Data Service"
        pilotjob6.cancel()
    ###########################################################################################################################

    ##########################################################################################################
    ##Program-7 - sp3
    ##########################################################################################################
    def pgm_sp3(self):
        print "\nsp3: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.small",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob7 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nsp3: Time to set up VM: ", tot_time1
        #-------------------------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program sp3 part-1
        #-------------------------------------------------------------------------------------------------
        print "\nsp3: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-sp3-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "sh",
            "arguments": [" sp3-chain.sh"],
            "environment": ["sparks=/usr/local/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_sp3.txt",
            "error": "stderr_chain_sp3.txt"
        }
        compute_unitc1 = pilotjob7.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("sp3-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sp3-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "sh",
            "arguments": [" sp3-domain.sh"],
            "environment": ["sparks=/usr/local/sparks","PATH=$PATH:$sparks/bin"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_sp3.txt",
            "error": "stderr_domain_sp3.txt"
        }
        compute_unitd1 = pilotjob7.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("sp3-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob7.wait()
        tot_time = time.time() - start_time
        print "sp3: part-1: input files:\t %s\n" %input_data_unit.list()
        print "sp3: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nsp3: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - sp3: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-sp3-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s F6VMN7-chain.sp3.out -a F6VMN7-chain.sp3 -o F6VMN7-chain.sp3.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_sp3_final.txt",
            "error": "stderr_chain_sp3_final.txt"
        }
        compute_unitc2 = pilotjob7.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("sp3-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-sp3-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-sp3",
            "arguments": [" -s F6VMN7-domain.sp3.out -a F6VMN7-domain.sp3 -o F6VMN7-domain.sp3.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_sp3_final.txt",
            "error": "stderr_domain_sp3_final.txt"
        }

        compute_unitd2 = pilotjob7.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("sp3: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "sp3: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob7.wait()
        tot_time = time.time() - start_time
        print "sp3: part-2: input files:\t %s\n" %input_data_unit.list()
        print "sp3: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nsp3: Time for part 2: ", tot_time
        logger.info("sp3: Terminate Pilot Compute/Data Service")
        print "\nsp3: Terminate Pilot Compute/Data Service"
        pilotjob7.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-8 - compass
    ##########################################################################################################
    def pgm_compass(self):
        print "\ncompass: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.medium",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob8 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\ncompass: Time to set up VM: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program compass part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-compass-blast part: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit for blast part-1
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/blast-2.2.25/bin/blastpgp",
            "arguments": [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/libs/uniprot/uniref90 -h 0.001 -j 5 -m 6 -o F6VMN7.blastout"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_compass-blast-1.txt",
            "error": "stderr_compass-blast-1.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        compute_unit.wait()
        print "compass-blast: input files:\t %s\n" %input_data_unit.list()
        print "compass-blast-part-1: output files:\t %s\n" %output_data_unit.list()

        # create compute unit for blast part-2
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "perl",
            "arguments": [" /usr/local/compass-3.1/get_last_br_iter_1.pl -i F6VMN7.blastout -o F6VMN7.aln1 -c /usr/local/compass-3.1/prep_psiblastali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_compass-blast-2.txt",
            "error": "stderr_compass-blast-2.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        compute_unit.wait()
        print "compass-blast-part-2: output files:\t %s\n" %output_data_unit.list()

        # create compute unit for blast part-3
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-fix1",
            "arguments": [" F6VMN7.aln1 F6VMN7.aln"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_compass-blast-3.txt",
            "error": "stderr_compass-blast-3.txt"
        }
        compute_unit = pilotjob8.submit_compute_unit(compute_unit_description)
        compute_unit.wait()
        print "compass-blast: output files:\t %s\n" %output_data_unit.list()

        #pilotjob8.wait()
        tot_time = time.time() - start_time
        print "\ncompass: Time for blast part: ", tot_time
        #********************************************************************************************#
        # Start compass after blast part                                                             #
        # Compute Unit Description12 - compass: Part-2                                               #
        #********************************************************************************************#
        print "\nProgram-compass part: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i F6VMN7.aln -d /usr/local/libraries/ethread-lib-latest/compass/chain.db -o F6VMN7-chain.compass"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_compass.txt",
            "error": "stderr_chain_compass.txt"
        }
        compute_unitc1 = pilotjob8.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("compass-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/compass-3.1/compass_vs_db",
            "arguments": [" -c 0 -e 100.0 -v 1000 -b 5000 -i F6VMN7.aln -d /usr/local/libraries/ethread-lib-latest/compass/domain.db -o F6VMN7-domain.compass"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_compass.txt",
            "error": "stderr_domain_compass.txt"
        }
        compute_unitd1 = pilotjob8.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("compass-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob8.wait()
        tot_time = time.time() - start_time
        print "compass: part-1: input files:\t %s\n" %input_data_unit.list()
        print "compass: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\ncompass: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - compass: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-compass",
            "arguments": [" -i F6VMN7-chain.compass -o F6VMN7-chain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_chain_compass_final.txt",
            "error": "stderr_chain_compass_final.txt"
        }
        compute_unitc2 = pilotjob8.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("compass-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-compass",
            "arguments": [" -i F6VMN7-domain.compass -o F6VMN7-domain.compass.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_domain_compass_final.txt",
            "error": "stderr_domain_compass_final.txt"
        }

        compute_unitd2 = pilotjob8.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("compass: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "compass: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob8.wait()
        tot_time = time.time() - start_time
        print "compass: part-2: input files:\t %s\n" %input_data_unit.list()
        print "compass: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\ncompass: Time for part 2: ", tot_time
        logger.info("compass: Terminate Pilot Compute/Data Service")
        print "\ncompass: Terminate Pilot Compute/Data Service"
        pilotjob8.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-9 - samt2k
    ##########################################################################################################
    def pgm_samt2k(self):
        print "\nsamt2k: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.large",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob9 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nsamt2k: Time to set up VM: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program samt2k part-1
        #----------------------------------------------------------------------------------
        print "\ntarget2k: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit for target2k part-1
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/target2k",
            "arguments": [" -seed F6VMN7.fasta -out F6VMN7-t2k"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-1.txt",
            "error": "stderr_target2k-1.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-1"
        compute_unit.wait()
        # create compute unit for target2k part-2
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/w0.5",
            "arguments": [" F6VMN7-t2k.a2m F6VMN7-t2k-w0.5.mod"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-2.txt",
            "error": "stderr_target2k-2.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-2"
        compute_unit.wait()
        # create compute unit for target2k part-3
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" F6VMN7-t2k -i F6VMN7-t2k-w0.5.mod -calibrate 1"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-3.txt",
            "error": "stderr_target2k-3.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-3"
        compute_unit.wait()
        # create compute unit for target2k part-4
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "sed",
            "arguments": [" -e s/insert\ /model_file\ /g F6VMN7-t2k.mlib > F6VMN7-t2k.fix | mv F6VMN7-t2k.fix F6VMN7-t2k.mlib"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [ { output_data_unit.get_url(): ["std*","F6VMN7*"] } ],
            "output": "stdout_target2k-4.txt",
            "error": "stderr_target2k-4.txt"
        }
        compute_unit = pilotjob9.submit_compute_unit(compute_unit_description)
        print "working target2k part-4"
        compute_unit.wait()

        print "target2k: input files:\t %s\n" %input_data_unit.list()
        print "target2k: output files:\t %s\n" %output_data_unit.list()

        tot_time = time.time() - start_time
        print "\nsamt2k: Time for target2k part: ", tot_time
        #********************************************************************************************#
        # Start samt2k after target2k part                                                           #
        # Compute Unit Description12 - samt2k: Part-2                                                #
        #********************************************************************************************#
        print "\nProgram-samt2k-part: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": ["ain -modellibrary F6VMN7-t2k.mlib -db /usr/local/libraries/ethread-lib-latest/blast/chain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5 | mv chain.1.F6VMN7-t2k-w0.5.mod.a2m F6VMN7-chain-t2k-w0.5.a2m | mv chain.1.F6VMN7-t2k-w0.5.mod.dist F6VMN7-chain-t2k-w0.5.dist"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_chain_samt2k.txt",
            "error": "stderr_chain_samt2k.txt"
        }
        compute_unitc1 = pilotjob9.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("samt2k-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/hmmscore",
            "arguments": [" domain -modellibrary F6VMN7-t2k.mlib -db /usr/local/libraries/ethread-lib-latest/blast/domain -select_align 8 -sw 2 -dpstyle 0 -adpstyle 5 | mv domain.1.F6VMN7-t2k-w0.5.mod.a2m F6VMN7-domain-t2k-w0.5.a2m | mv domain.1.F6VMN7-t2k-w0.5.mod.dist F6VMN7-domain-t2k-w0.5.dist"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_domain_samt2k.txt",
            "error": "stderr_domain_samt2k.txt"
        }
        compute_unitd1 = pilotjob9.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("samt2k-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob9.wait()
        tot_time = time.time() - start_time
        print "samt2k: part-1: input files:\t %s\n" %input_data_unit.list()
        print "samt2k: part-1: output files:\t %s\n" %output_data_unit.list()
        print "samt2k\n: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - samt2k: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" F6VMN7-chain-t2k-w0.5.a2m -l 10000 > F6VMN7-chain-t2k-w0.5.aln"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_chain_samt2k_2.txt",
            "error": "stderr_chain_samt2k_2.txt"
        }
        compute_unitc2 = pilotjob9.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("samt2k-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unitc2.wait()

        # create compute unit-chain-part3
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t F6VMN7.fasta -s F6VMN7-chain-t2k-w0.5.dist -a F6VMN7-chain-t2k-w0.5.aln -o F6VMN7-chain.samt2k.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_chain_samt2k_final.txt",
            "error": "stderr_chain_samt2k_final.txt"
        }
        compute_unitc2 = pilotjob9.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("samt2k-chain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-chain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/sam3.5.x86_64-linux/bin/prettyalign",
            "arguments": [" F6VMN7-domain-t2k-w0.5.a2m -l 10000 > F6VMN7-domain-t2k-w0.5.aln"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_domain_samt2k_2.txt",
            "error": "stderr_domain_samt2k_2.txt"
        }
       
        compute_unitd2 = pilotjob9.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("samt2k-domain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-domain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"
        compute_unitd2.wait()

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-samt2k",
            "arguments": [" -t F6VMN7.fasta -s F6VMN7-domain-t2k-w0.5.dist -a F6VMN7-domain-t2k-w0.5.aln -o F6VMN7-domain.samt2k.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                ],
            "output": "stdout_domain_samt2k_final.txt",
            "error": "stderr_domain_samt2k_final.txt"
        }

        compute_unitd2 = pilotjob9.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("samt2k-domain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "samt2k-domain-Part-3: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob9.wait()
        tot_time = time.time() - start_time
        print "samt2k: part-2: input files:\t %s\n" %input_data_unit.list()
        print "samt2k: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nsamt2k: Time for part 2: ", tot_time
        logger.info("samt2k: Terminate Pilot Compute/Data Service")
        print "\nsamt2k: Terminate Pilot Compute/Data Service"
        pilotjob9.cancel()
        ###################################################################################################

        #***************************************************************************
        #*All the 11 programs can be written before the start of the go() function * 
        #*Essentially by copying the pgm_csblast() and modifying it as required    *
        #***************************************************************************

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called, s3cmd collects the data to local sys
    ########################################################################################################
    def go(self):
        threads=[]
        '''for i in range(9):
            #Add programs as threads
            if i==0:
                thread=threading.Thread(target=self.pgm_csblast)
                thread.start()
            if i==1:
                thread=threading.Thread(target=self.pgm_pftools)
                thread.start()
            if i==2:
                thread=threading.Thread(target=self.pgm_hmmer)
                thread.start()
            if i==3:
                thread=threading.Thread(target=self.pgm_psipred_threader)
                thread.start()
            if i==4:
                thread=threading.Thread(target=self.pgm_hhpred)
                thread.start()
            if i==5:
                thread=threading.Thread(target=self.pgm_sparks)
                thread.start()
            if i==6:
                thread=threading.Thread(target=self.pgm_sp3)
                thread.start()
            if i==7:
                thread=threading.Thread(target=self.pgm_compass)
                thread.start()
            if i==8:
                thread=threading.Thread(target=self.pgm_samt2k)
                thread.start()
            
            threads.append(thread)

        for thread in threads:
            thread.join()'''

        #To run individual program during testing
        for i in range(1):
            #Add programs as threads
            if i==0:
                thread=threading.Thread(target=self.pgm_csblast)
                thread.start()
                thread.join()

    #########################################################################################################            
        
    ####################################################################################################################
    #ethread starts here
    ####################################################################################################################
    def ethread(self):
        print "\neThread Starts here (temp disabled)"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()

        '''###comment starts here
        print "\nethread: Setting PCD-ethread"
        pilot_compute_description_amazon_west = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west)

        tot_time = time.time() - start_time
        print "\nethread: Time to set up VM: ", tot_time

        # create compute unit
        start_time = time.time()
        compute_unit_description = {
            "executable": "/bin/date",
            "arguments": [],
            "number_of_processes": 1,
            "output": "stdout_ethread.txt",
            "error": "stderr_ethread.txt",
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                            {
                             output_data_unit.get_url():
                             ["std*"]
                            }
                           ]
        }

        compute_unit = pilotjob.submit_compute_unit(compute_unit_description)
        logger.info("Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "\neThread: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        compute_unit.wait()
        tot_time = time.time() - start_time
        print "\nTime for eThread part: ", tot_time
        pilotjob.cancel()
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print "Input data URL : %s" %input_data_unit.get_url()
        print "Output data URL: %s" %output_data_unit.get_url()
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        ###comment till here'''


        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "This is url of S3"
        print pd.service_url
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "\nExport S3 data to local\n"

        e = os.system('s3cmd get -r %s' %pd.service_url)

        if not e == 0:
            print >> sys.stderr, 'Error on fetching data:', e
        #pd.export_du(pd,os.getcwd())
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"


        logger.debug("Output Data Unit: " + str(output_data_unit.list()))
        #delete the s3 bucket if you do not want to keep in s3
        #e = os.system('s3cmd rb -rf %s' %pd.service_url)

        #cancel pilot compute/data services
        pilot_data_service.cancel() 
        pilot_compute_service.cancel()
             
#################################################################################################################################

#Object to the class mainThread()
obj = mainThread()
#To call each thread
obj.go()
#ethread will execute after all threads complete
obj.ethread()
