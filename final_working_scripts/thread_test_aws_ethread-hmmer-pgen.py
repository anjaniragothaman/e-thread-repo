import threading
import os
import time
import sys
import logging
import uuid

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

# create pilot data service (factory for data pilots (physical, distributed storage))
# and pilot data
###################################################################################################
##Pilot-data Service
###################################################################################################
print "\nSetting up Pilot Data Service in S3"

pilot_data_service = PilotDataService(coordination_url=COORD)
pilot_data_description_aws={
                                "service_url": "s3://pilot-data-" + str(uuid.uuid1()),
                                "size": 100,
                                "access_key_id":ACCESS_KEY_ID,
                                "secret_access_key":SECRET_ACCESS_KEY
                           }

pd = pilot_data_service.create_pilot(pilot_data_description=pilot_data_description_aws)

#-------------------------------------------------------------------------------------------------#
# Put all input files, that will be upload  to S3  
data_unit_description = { "file_urls": [os.path.join(os.getcwd(), "data/F6VMN7.fasta"), os.path.join(os.getcwd(), "data/test2.txt"), os.path.join(os.getcwd(), "test3.txt")] }
#-------------------------------------------------------------------------------------------------#

# Load input data into PD(PD for AWS is S3) 
input_data_unit = pd.submit_data_unit(data_unit_description)
input_data_unit.wait()
logger.info("Input Data Unit URL: " + input_data_unit.get_url())
print "\nData Unit URL: " + input_data_unit.get_url()

# create empty data unit for output data
output_data_unit_description = { "file_urls": [] }
output_data_unit = pd.submit_data_unit(output_data_unit_description)
output_data_unit.wait()
logger.info("Output Data Unit URL: " + output_data_unit.get_url())
print "\nOutput Data Unit URL: " + output_data_unit.get_url()
#########################################################################################################
#########################################################################################################
#compute_data_service = ComputeDataService()
pilot_compute_service = PilotComputeService(coordination_url=COORD)

class mainThread:
    def __init__(self):
        self.running = True

    ##########################################################################################################
    ##Program-1 - CSBLAST
    ##########################################################################################################
    def pgm_hmmer(self):
        print "\nPgm-1: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nPgm-1: Time to set up VM1: ", tot_time1
        ###################################################################################################
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        ###################################################################################################
        print "\nProgram-1-hmmer: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-chain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_chain_hmmer.txt",
            "error": "stderr_chain_hmmer.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-domain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_domain_hmmer.txt",
            "error": "stderr_domain_hmmer.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-1: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-chain.hmmer -o F6VMN7-chain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_hmmer_final.txt",
            "error": "stderr_chain_hmmer_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-domain.hmmer -o F6VMN7-domain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_hmmer_final.txt",
            "error": "stderr_domain_hmmer_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-2: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 2: ", tot_time
        logger.info("hmmer: Terminate Pilot Compute/Data Service")
        print "\nhmmer: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-2 - pftools
    ##########################################################################################################
    def pgm_pgen(self):
        print "\npgt: Creating VM instance"
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        start_time = time.time()
        pilot_compute_description_amazon_west2 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"m1.medium",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob2 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        print "\npgen: Time to set up VM2: ", tot_time
        ###################################################################################################
        #Cumpute Unit Description - program pftools part-1
        ###################################################################################################
        print "\nProgram-2-pgen: compute unit starts here"
        start_time = time.time()
        # create compute unit-pgen-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/genthreader/pGenThreader-chain.sh",
            "arguments": [" pgt-F6VMN7-chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pgen*","*pgt*"]
                        }
                 ],
            "output": "stdout_chain_pgen.txt",
            "error": "stderr_chain_pgen.txt"
        }
        compute_unitc1 = pilotjob2.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("pgen-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pgen-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pgen-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/genthreader/pGenThreader-domain.sh",
            "arguments": [" pgt-F6VMN7-domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pgt*","*pgen*"]
                        }
                 ],
            "output": "stdout_domain_pgen.txt",
            "error": "stderr_domain_pgen.txt"
        }
        compute_unitd1 = pilotjob2.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("pgen-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pgen-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "pgen: part-1: input files:\t %s\n" %input_data_unit.list()
        print "pgen: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\npgen: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - pgen: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-pgen-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pgenthreader",
            "arguments": [" -s pgt-F6VMN7-chain.pgen.presults -a pgt-F6VMN7-chain.pgen.align -o F6VMN7-chain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_pgen_final.txt",
            "error": "stderr_chain_pgen_final.txt"
        }
        compute_unitc2 = pilotjob2.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("pgen-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pgen-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pgen-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pgenthreader",
            "arguments": [" -s pgt-F6VMN7-domain.pgen.presults -a pgt-F6VMN7-domain.pgen.align -o F6VMN7-domain.pgenthreader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_pgen_final.txt",
            "error": "stderr_domain_pgen_final.txt"
        }

        compute_unitd2 = pilotjob2.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("pgen: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pgen: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob2.wait()

        tot_time = time.time() - start_time
        print "\npgen: Time for part 2: ", tot_time
        logger.info("pgen: Terminate Pilot Compute/Data Service")
        print "\npgen: Terminate Pilot Compute/Data Service"
        pilotjob2.cancel()
    ###########################################################################################################################
    #***************************************************************************
    #*All the 11 progrmas can be written before the start of the go() function * 
    #*Essentially by copying the pgm_csblast() and modifying it as required    *
    #***************************************************************************

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called, s3cmd collects the data to local sys
    ########################################################################################################
    def go(self):
        threads=[]
        for i in range(2):
            #if i==0:
             #   thread=threading.Thread(target=self.pgm_hmmer)
              #  thread.start()
            if i==1:
                thread=threading.Thread(target=self.pgm_pgen)
                thread.start()
                thread.join()
            #Add remaining programs as threads
            
#            threads.append(thread)

 #       for thread in threads:
  #          thread.join()
    #########################################################################################################            
        
    ####################################################################################################################
    #ethread starts here
    ####################################################################################################################
    def ethread(self):
        print "\neThread Starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()

        print "\nethread: Setting PCD-ethread"
        pilot_compute_description_amazon_west = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west)

        tot_time = time.time() - start_time
        print "\nethread: Time to set up VM: ", tot_time

        # create compute unit
        start_time = time.time()
        compute_unit_description = {
            "executable": "/bin/date",
            "arguments": [],
            "number_of_processes": 1,
            "output": "stdout_ethread.txt",
            "error": "stderr_ethread.txt",
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                            {
                             output_data_unit.get_url():
                             ["std*"]
                            }
                           ]
        }

        compute_unit = pilotjob.submit_compute_unit(compute_unit_description)
        logger.info("Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "\neThread: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        compute_unit.wait()
        tot_time = time.time() - start_time
        print "\nTime for eThread part: ", tot_time

        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print "Input data URL : %s" %input_data_unit.get_url()
        print "Output data URL: %s" %output_data_unit.get_url()
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"


        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "This is url of S3"
        print pd.service_url
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "Export S3 data to local"

        e = os.system('s3cmd get -r %s' %pd.service_url)

        if not e == 0:
            print >> sys.stderr, 'Error on fetching data:', e
        #pd.export_du(pd,os.getcwd())
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"


        logger.debug("Output Data Unit: " + str(output_data_unit.list()))
#        print "Output Data Unit: " + str(output_data_unit.list())
    
        pilot_data_service.cancel()
        pilot_compute_service.cancel()
        

obj = mainThread()
obj.go()
obj.ethread()
