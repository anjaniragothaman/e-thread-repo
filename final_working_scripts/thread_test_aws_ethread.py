import threading
import os
import time
import sys
import logging
import uuid

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

# create pilot data service (factory for data pilots (physical, distributed storage))
# and pilot data
###################################################################################################
##Pilot-data Service
###################################################################################################
print "\nSetting up Pilot Data Service in S3"

pilot_data_service = PilotDataService(coordination_url=COORD)
pilot_data_description_aws={
                                "service_url": "s3://pilot-data-" + str(uuid.uuid1()),
                                "size": 100,
                                "access_key_id":ACCESS_KEY_ID,
                                "secret_access_key":SECRET_ACCESS_KEY
                           }

pd = pilot_data_service.create_pilot(pilot_data_description=pilot_data_description_aws)

#-------------------------------------------------------------------------------------------------#
# Put all input files, that will be upload  to S3  
data_unit_description = { "file_urls": [os.path.join(os.getcwd(), "data/F6VMN7.fasta"), os.path.join(os.getcwd(), "data/test2.txt"), os.path.join(os.getcwd(), "test3.txt")] }
#-------------------------------------------------------------------------------------------------#

# Load input data into PD(PD for AWS is S3) 
input_data_unit = pd.submit_data_unit(data_unit_description)
input_data_unit.wait()
logger.info("Input Data Unit URL: " + input_data_unit.get_url())
print "\nData Unit URL: " + input_data_unit.get_url()

#########################################################################################################
#########################################################################################################
#compute_data_service = ComputeDataService()
pilot_compute_service = PilotComputeService(coordination_url=COORD)

class mainThread:
    def __init__(self):
        self.running = True

    ##########################################################################################################
    ##Program-1 - CSBLAST
    ##########################################################################################################
    def pgm_csblast(self):
        print "\ncsblast: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\ncsblast: Time to set up VM1: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-1-CS BLAST: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/chain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-chain.csblast"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.csblast"]
                        }
                 ],
            "output": "stdout_chain_blast.txt",
            "error": "stderr_chain_blast.txt"
        }
        compute_unitc1 = pilotjob1.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("csblast-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/csblast-2.1.0-linux64/csblast_static",
            "arguments": [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/domain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-domain.csblast"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.csblast"]
                        }
                 ],
            "output": "stdout_domain_blast.txt",
            "error": "stderr_domain_blast.txt"
        }
        compute_unitd1 = pilotjob1.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-1: input files:\t %s\n" %input_data_unit.list()
        print "csblast: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\ncsblast: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-chain.csblast -o F6VMN7-chain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_blast_final.txt",
            "error": "stderr_chain_blast_final.txt"
        }
        compute_unitc2 = pilotjob1.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-csiblast",
            "arguments": ["-i F6VMN7-domain.csblast -o F6VMN7-domain.csblast.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_blast_final.txt",
            "error": "stderr_domain_blast_final.txt"
        }

        compute_unitd2 = pilotjob1.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "csblast: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "csblast: part-2: input files:\t %s\n" %input_data_unit.list()
        print "csblast: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\ncsblast: Time for part 2: ", tot_time
        logger.info("csblast: Terminate Pilot Compute/Data Service")
        print "\ncsblast: Terminate Pilot Compute/Data Service"
        pilotjob1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-2 - pftools
    ##########################################################################################################
    def pgm_pftools(self):
        print "\npftools: Creating VM instance"
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        start_time = time.time()
        pilot_compute_description_amazon_west2 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob2 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        print "\npftools: Time to set up VM2: ", tot_time
        #----------------------------------------------------------------------------------
        #Cumpute Unit Description - program pftools part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-2-pf tools: compute unit starts here"
        start_time = time.time()
        # create compute unit-pftools-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/bin/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/chain.prf > F6VMN7-chain.pftools"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_chain_pftools.txt",
            "error": "stderr_chain_pftools.txt"
        }
        compute_unitc1 = pilotjob2.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/bin/pfscan",
            "arguments": [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/domain.prf > F6VMN7-domain.pftools"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["*pftools*"]
                        }
                 ],
            "output": "stdout_domain_pftools.txt",
            "error": "stderr_domain_pftools.txt"
        }
        compute_unitd1 = pilotjob2.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("pftools-domain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "pftools: part-1: input files:\t %s\n" %input_data_unit.list()
        print "pftools: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\npftools: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description - pftools: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-pftools-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-chain.pftools -o F6VMN7-chain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/chain.map"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_pftools_final.txt",
            "error": "stderr_chain_pftools_final.txt"
        }
        compute_unitc2 = pilotjob2.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-pftools-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-pftools",
            "arguments": ["-i F6VMN7-domain.pftools -o F6VMN7-domain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/domain.map"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_pftools_final.txt",
            "error": "stderr_domain_pftools_final.txt"
        }

        compute_unitd2 = pilotjob2.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "pftools: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob2.wait()

        tot_time = time.time() - start_time
        print "\npftools: Time for part 2: ", tot_time
        logger.info("pftools: Terminate Pilot Compute/Data Service")
        print "\npftools: Terminate Pilot Compute/Data Service"
        pilotjob2.cancel()
    ###########################################################################################################################

    ##########################################################################################################
    ##Program-3 - hmmer
    ##########################################################################################################
    def pgm_hmmer(self):
        print "\nhmmer: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob3 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\nhmmer: Time to set up VM1: ", tot_time1
        #-------------------------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program hmmer part-1
        #-------------------------------------------------------------------------------------------------
        print "\nhmmer: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit-hmmer-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-chain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/chain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_chain_hmmer.txt",
            "error": "stderr_chain_hmmer.txt"
        }
        compute_unitc1 = pilotjob3.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hmmer-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/hmmer-3.0-linux-intel-x86_64/binaries/hmmscan",
            "arguments": [" -o F6VMN7-domain.hmmer --notextw -E 100.0 --max /usr/local/libraries/ethread-lib-latest/hmmer/domain F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.hmmer"]
                        }
                 ],
            "output": "stdout_domain_hmmer.txt",
            "error": "stderr_domain_hmmer.txt"
        }
        compute_unitd1 = pilotjob3.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob3.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-1: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - hmmer: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-hmmer-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-chain.hmmer -o F6VMN7-chain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_chain_hmmer_final.txt",
            "error": "stderr_chain_hmmer_final.txt"
        }
        compute_unitc2 = pilotjob3.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-hmmer-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-hmmer",
            "arguments": ["-i F6VMN7-domain.hmmer -o F6VMN7-domain.hmmer.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali"]
                        }
                ],
            "output": "stdout_domain_hmmer_final.txt",
            "error": "stderr_domain_hmmer_final.txt"
        }

        compute_unitd2 = pilotjob3.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "hmmer: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob3.wait()
        tot_time = time.time() - start_time
        print "hmmer: part-2: input files:\t %s\n" %input_data_unit.list()
        print "hmmer: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nhmmer: Time for part 2: ", tot_time
        logger.info("hmmer: Terminate Pilot Compute/Data Service")
        print "\nhmmer: Terminate Pilot Compute/Data Service"
        pilotjob3.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-4 - psipred and Threader
    ##########################################################################################################
    def pgm_psipred_threader(self):
        print "\npsipred: Creating VM instance"
        start_time1 = time.time()
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob4 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        tot_time1 = time.time() - start_time1
        print "\npsipred: Time to set up VM1: ", tot_time1
        #----------------------------------------------------------------------------------
        #1st CU: Cumpute Unit Description11 - program psipred part-1
        #----------------------------------------------------------------------------------
        print "\nProgram-4-psipred: compute unit starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()
        # create compute unit
        compute_unit_description = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/psipred321/runpsipred",
            "arguments": [" F6VMN7.fasta"],
            "number_of_processes": 1,
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","F6VMN7*"]
                        }
                 ],
            "output": "stdout_psipred.txt",
            "error": "stderr_psipred.txt"
        }
        compute_unit = pilotjob4.submit_compute_unit(compute_unit_description)
        logger.info("psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "psipred: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "psipred: input files:\t %s\n" %input_data_unit.list()
        print "psipred: output files:\t %s\n" %output_data_unit.list()
        print "\npsipred: Time for psipred: ", tot_time
        #********************************************************************************************#
        # Start Threader after psipred                                                               #
        # Compute Unit Description12 - threader: Part-2                                              #
        #********************************************************************************************#
        print "\nProgram-4-Threader part: compute unit starts here"
        start_time = time.time()
        # create compute unit-chain-part1
        compute_unit_descriptionc1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-chain.threader.out /usr/local/libraries/ethread-lib-latest/threader/chain.lst > F6VMN7-chain.threader"],
            "environment": ["THREAD_DIR=/usr/local/threader","TDB_DIR=/usr/local/libraries/ethread-lib-latest/threader/chain"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_chain_threader.txt",
            "error": "stderr_chain_threader.txt"
        }
        compute_unitc1 = pilotjob4.submit_compute_unit(compute_unit_descriptionc1)
        logger.info("threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part1
        compute_unit_descriptiond1 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/threader/threader-linux",
            "arguments": ["-d 200 -p -j F6VMN7.horiz F6VMN7-domain.threader.out /usr/local/libraries/ethread-lib-latest/threader/domain.lst > F6VMN7-domain.threader"],
            "environment": ["THREAD_DIR=/usr/local/threader","TDB_DIR=/usr/local/libraries/ethread-lib-latest/threader/domain"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*threader*"]
                        }
                 ],
            "output": "stdout_domain_threader.txt",
            "error": "stderr_domain_threader.txt"
        }
        compute_unitd1 = pilotjob4.submit_compute_unit(compute_unit_descriptiond1)
        logger.info("threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-doamin-part1: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-1: input files:\t %s\n" %input_data_unit.list()
        print "threader: part-1: output files:\t %s\n" %output_data_unit.list()
        print "\nthreader: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        #-------------------------------------------------------------------------------------------------#
        start_time = time.time()
        # create compute unit-chain-part2
        compute_unit_descriptionc2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-chain.threader.out -a F6VMN7-chain.threader -o F6VMN7-chain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_chain_threader_final.txt",
            "error": "stderr_chain_threader_final.txt"
        }
        compute_unitc2 = pilotjob4.submit_compute_unit(compute_unit_descriptionc2)
        logger.info("threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader-chain-Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        # create compute unit-domain-part2
        compute_unit_descriptiond2 = {
            "working_directory": "/home/ubuntu/NY/",
            "executable": "/usr/local/ethread-1.0/bin/econv-threader",
            "arguments": ["-s F6VMN7-domain.threader.out -a F6VMN7-domain.threader -o F6VMN7-domain.threader.ali"],
            "number_of_processes": 1,
            "input_data": [output_data_unit.get_url()],
            "output_data": [
                        {
                                output_data_unit.get_url():
                                ["std*","*.ali*"]
                        }
                ],
            "output": "stdout_domain_threader_final.txt",
            "error": "stderr_domain_threader_final.txt"
        }

        compute_unitd2 = pilotjob4.submit_compute_unit(compute_unit_descriptiond2)
        logger.info("threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "threader: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        output_data_unit.wait()
        pilotjob4.wait()
        tot_time = time.time() - start_time
        print "threader: part-2: input files:\t %s\n" %input_data_unit.list()
        print "threader: part-2: output files:\t %s\n" %output_data_unit.list()
        print "\nthreader: Time for part 2: ", tot_time
        logger.info("psipred-threader: Terminate Pilot Compute/Data Service")
        print "\npsipred-threader: Terminate Pilot Compute/Data Service"
        pilotjob4.cancel()
        ###################################################################################################

    #***************************************************************************
    #*All the 11 progrmas can be written before the start of the go() function * 
    #*Essentially by copying the pgm_csblast() and modifying it as required    *
    #***************************************************************************

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called, s3cmd collects the data to local sys
    ########################################################################################################
    def go(self):
        threads=[]
        for i in range(4):
            if i==0:
                thread=threading.Thread(target=self.pgm_csblast)
                thread.start()
            if i==1:
                thread=threading.Thread(target=self.pgm_pftools)
                thread.start()
            if i==2:
                thread=threading.Thread(target=self.pgm_hmmer)
                thread.start()
            if i==3:
                thread=threading.Thread(target=self.pgm_psipred_threader)
                thread.start()
            #Add remaining programs as threads
            
            threads.append(thread)

        for thread in threads:
            thread.join()
    #########################################################################################################            
        
    ####################################################################################################################
    #ethread starts here
    ####################################################################################################################
    def ethread(self):
        print "\neThread Starts here (temp disabled)"
        '''###comment starts here
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()

        print "\nethread: Setting PCD-ethread"
        pilot_compute_description_amazon_west = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west)

        tot_time = time.time() - start_time
        print "\nethread: Time to set up VM: ", tot_time

        # create compute unit
        start_time = time.time()
        compute_unit_description = {
            "executable": "/bin/date",
            "arguments": [],
            "number_of_processes": 1,
            "output": "stdout_ethread.txt",
            "error": "stderr_ethread.txt",
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                            {
                             output_data_unit.get_url():
                             ["std*"]
                            }
                           ]
        }

        compute_unit = pilotjob.submit_compute_unit(compute_unit_description)
        logger.info("Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "\neThread: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        compute_unit.wait()
        tot_time = time.time() - start_time
        print "\nTime for eThread part: ", tot_time
        pilotjob.cancel()
        ###comment till here'''
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print "Input data URL : %s" %input_data_unit.get_url()
        print "Output data URL: %s" %output_data_unit.get_url()
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"


        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "This is url of S3"
        print pd.service_url
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "\nExport S3 data to local\n"

        e = os.system('s3cmd get -r %s' %pd.service_url)

        if not e == 0:
            print >> sys.stderr, 'Error on fetching data:', e
        #pd.export_du(pd,os.getcwd())
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"


        logger.debug("Output Data Unit: " + str(output_data_unit.list()))
        #cancel pilot compute/data services
        pilot_data_service.cancel() 
        pilot_compute_service.cancel()
    
#################################################################################################################################

#Object to the class mainThread()
obj = mainThread()
#To call each thread
obj.go()
#ethread will execute after all threads complete
obj.ethread()
