import threading
import os
import time
import sys
import logging
import uuid

from pilot import PilotComputeService, PilotDataService, ComputeDataService, State
from bigjob import logger

#------------------------------------------------------------------------------
# Redis password and eThread secret key details aquired from the environment
COORD = os.environ.get('COORDINATION_URL')
ACCESS_KEY_ID = os.environ.get('ETHREAD_ACCESS_KEY_ID')
SECRET_ACCESS_KEY= os.environ.get('ETHREAD_SECRET_ACCESS_KEY')
#------------------------------------------------------------------------------

# create pilot data service (factory for data pilots (physical, distributed storage))
# and pilot data
###################################################################################################
##Pilot-data Service
###################################################################################################
print "\nSetting up Pilot Data Service in S3"
start_time = time.time()

pilot_data_service = PilotDataService(coordination_url=COORD)
pilot_data_description_aws={
                                "service_url": "s3://pilot-data-" + str(uuid.uuid1()),
                                "size": 100,
                                "access_key_id":ACCESS_KEY_ID,
                                "secret_access_key":SECRET_ACCESS_KEY
                           }

pd = pilot_data_service.create_pilot(pilot_data_description=pilot_data_description_aws)

#-------------------------------------------------------------------------------------------------#
# Put all input files, that will be upload  to S3  
data_unit_description = { "file_urls": [os.path.join(os.getcwd(), "data/F6VMN7.fasta"), os.path.join(os.getcwd(), "data/test2.txt"), os.path.join(os.getcwd(), "test3.txt")] }
#-------------------------------------------------------------------------------------------------#

# Load input data into PD(PD for AWS is S3) 
input_data_unit = pd.submit_data_unit(data_unit_description)
input_data_unit.wait()
tot_time = time.time() - start_time
print "\nTime to set up pilot data: ", tot_time

logger.info("Data Unit URL: " + input_data_unit.get_url())
print "\nData Unit URL: " + input_data_unit.get_url()
#########################################################################################################
#########################################################################################################
#compute_data_service = ComputeDataService()
pilot_compute_service = PilotComputeService(coordination_url=COORD)

class mainThread:
    def __init__(self):
        self.running = True

    ##########################################################################################################
    ##Program-1 - CSBLAST
    ##########################################################################################################
    def pgm_csblast(self):
        print "\nPgm-1: Creating VM instance"
#        print "\nPgm-1: Setting PCD-1"
        start_time1 = time.time()
        #pilot_compute_service = PilotComputeService(coordination_url=COORD)
        pilot_compute_description_amazon_west1 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob1 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west1)

        #compute_data_service1 = ComputeDataService()
        #pilotjob1.add_pilot_compute_service(pilot_compute_service)
        #pilotjob1.add_pilot_data_service(pilot_data_service)

        tot_time1 = time.time() - start_time1
        print "\nPgm-1: Time to set up VM1: ", tot_time1
        ###################################################################################################
        #1st CU: Cumpute Unit Description11 - program CS BLAST part-1
        ###################################################################################################
        #-------------------------------------------------------------------------------------------------#
        # For multiple output
        #-------------------------------------------------------------------------------------------------#
        print "\nProgram-1-CS BLAST: compute unit starts here"
        outdus11=[]
        start_time = time.time()
        for i in range(2):
            # create empty data unit for output data
            output_data_unit_description = { "file_urls": [] }
            output_data_unit = pd.submit_data_unit(output_data_unit_description)
            output_data_unit.wait()
            outdus11.append(output_data_unit)

            # create compute unit
            compute_unit_description11 = {
                "working_directory": "/home/ubuntu/NY/",
                "executable": "/usr/local/csblast-2.1.0-linux64/csblast_static",
                "number_of_processes": 1,
                "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
                "output_data": [
                        {
                                outdus11[i].get_url():
                                ["std-1*", "*csblast*"]
                        }
                 ]

                }
            if i==0:
                compute_unit_description11["arguments"] = [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/chain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-chain.csblast"]
                compute_unit_description11["output"] = "std-1out_chain_blast.txt"
                compute_unit_description11["error"] = "std-1err_chain_blast.txt"

            elif i==1:
                compute_unit_description11["arguments"] = [" -i F6VMN7.fasta -d /usr/local/libraries/ethread-lib-latest/blast/domain -D /usr/local/csblast-2.1.0-linux64/K4000.lib -j 5 -h 0.002 -e 100.0 --blast-path /usr/local/blast-2.2.25/bin/ -o F6VMN7-domain.csblast"]
                compute_unit_description11["output"] = "std-1out_domain_blast.txt"
                compute_unit_description11["error"] = "std-1err_domain_blast.txt"

            compute_unit11 = pilotjob1.submit_compute_unit(compute_unit_description11)
            logger.info("PGM-1: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print ("\nPGM-1: Part 1: Finished setup of ComputeDataService %s. Waiting for scheduling of PD" % i)

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "bla\t %s\n" %outdus11
        print "csblast: part-1: chain output files:\t %s\n" %outdus11[0].list()
        print "csblast: part-1: domain output files:\t %s\n" %outdus11[1].list()
        print "\nPGM-1: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        # For multiple output
        #-------------------------------------------------------------------------------------------------#
        outdus12=[]
        start_time = time.time()
        for i in range(2):
            # create empty data unit for output data
            output_data_unit_description = { "file_urls": [] }
            output_data_unit = pd.submit_data_unit(output_data_unit_description)
            output_data_unit.wait()
            outdus12.append(output_data_unit)

            # create compute unit
            compute_unit_description12 = {
                "working_directory": "/home/ubuntu/NY/",
                "executable": "/usr/local/ethread-1.0/bin/econv-csiblast",
                "number_of_processes": 1,
                "input_data": [outdus11[i].get_url()],
                "output_data": [
                        {
                                outdus12[i].get_url():
                                ["*csblast*"]
                        }
                ]
            }

            if i==0:
                compute_unit_description12["arguments"] = ["-i F6VMN7-chain.csblast -o F6VMN7-chain.csblast.ali"]
                compute_unit_description12["output"] = "std-1out_chain_blast_final.txt"
                compute_unit_description12["error"] = "std-1err_chain_blast_final.txt"
                print "csblast: part-2: chain input files: %s\n" %outdus11[i].list()

            elif i==1:
                compute_unit_description12["arguments"] = ["-i F6VMN7-domain.csblast -o F6VMN7-domain.csblast.ali"]
                compute_unit_description12["output"] = "std-1out_domain_blast_final.txt"
                compute_unit_description12["error"] = "std-1err_domain_blast_final.txt"
                print "csblast: part-2: domain input files: %s\n" %outdus11[i].list()

            compute_unit12 = pilotjob1.submit_compute_unit(compute_unit_description12)
            logger.info("PGM-1: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print ("\nPGM-1: Part2: Finished setup of ComputeDataService %s. Waiting for scheduling of PD" % i)

        pilotjob1.wait()
        tot_time = time.time() - start_time
        print "\nPGM-1: Time for part 2: ", tot_time
        logger.info("PGM-1: Terminate Pilot Compute/Data Service")
        print "\nPGM-1: Terminate Pilot Compute/Data Service"
        #compute_data_service1.cancel()
        #pilot_compute_service1.cancel()
        ###################################################################################################

    ##########################################################################################################
    ##Program-2 - pftools(temp csblast for threads testing)
    ##########################################################################################################
    def pgm_pftools(self):
        print "\nPgm-2: Creating VM instance"
        start_time = time.time()
        pilot_compute_description_amazon_west2 = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob2 = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west2)

        tot_time = time.time() - start_time
        print "\nPgm-2: Time to set up VM2: ", tot_time
        ###################################################################################################
        #2nd CU: Cumpute Unit Description21 - program CS BLAST part-1
        ###################################################################################################
        #-------------------------------------------------------------------------------------------------#
        # For multiple output
        #-------------------------------------------------------------------------------------------------#
        print "\nProgram-2-pf tools: compute unit starts here"
        outdus21=[]
        start_time = time.time()
        for i in range(2):
            # create empty data unit for output data
            output_data_unit_description = { "file_urls": [] }
            output_data_unit = pd.submit_data_unit(output_data_unit_description)
            output_data_unit.wait()
            outdus21.append(output_data_unit)

            # create compute unit
            compute_unit_description21 = {
                "working_directory": "/home/ubuntu/NY/",
                "executable": "/usr/bin/pfscan",
                "number_of_processes": 1,
                "input_data": [input_data_unit.get_url()],
                # Put files stdout.txt and stderr.txt into output data unit
                "output_data": [
                        {
                                outdus21[i].get_url():
                                [ "*pftools*"]
                        }
                 ]

                }
            if i==0:
                compute_unit_description21["arguments"] = [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/chain.prf > F6VMN7-chain.pftools"]
                compute_unit_description21["output"] = "stdout_chain_pftools.txt"
                compute_unit_description21["error"] = "stderr_chain_pftools.txt"
                print "pftools: part1: output chain: %s" %outdus21[i].list()

            elif i==1:
                compute_unit_description21["arguments"] = [" -a -x -f F6VMN7.fasta /usr/local/libraries/ethread-lib-latest/pftools/domain.prf > F6VMN7-domain.pftools"]
                compute_unit_description21["output"] = "stdout_domain_pftools.txt"
                compute_unit_description21["error"] = "stderr_domain_pftools.txt"
                print "pftools: part1: output domain: %s" %outdus21[i].list()

            compute_unit21 = pilotjob2.submit_compute_unit(compute_unit_description21)
            logger.info("PGM-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print ("\nPGM-2: Part 1: Finished setup of ComputeDataService %s. Waiting for scheduling of PD" % i)

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "pftools: part1: chain output files:\t %s\n" %outdus21[0].list()
        print "pftools: part1: domain output files:\t %s\n" %outdus21[1].list()
        print "\nPGM-2: Time for part 1: ", tot_time
        #-------------------------------------------------------------------------------------------------#
        # Start file converting after finishing job, if it is needed
        # Compute Unit Description12 - CS BLAST: Part-2
        # For multiple output
        #-------------------------------------------------------------------------------------------------#
        outdus22=[]
        start_time = time.time()
        for i in range(2):
            # create empty data unit for output data
            output_data_unit_description = { "file_urls": [] }
            output_data_unit = pd.submit_data_unit(output_data_unit_description)
            output_data_unit.wait()
            outdus22.append(output_data_unit)

            # create compute unit
            compute_unit_description22 = {
                "working_directory": "/home/ubuntu/NY/",
                "executable": "/usr/local/ethread-1.0/bin/econv-pftools",
                "number_of_processes": 1,
                "input_data": [outdus21[i].get_url()],
                "output_data": [
                        {
                                outdus22[i].get_url():
                                [ "*pftools*"]
                        }
                ]
            }

            if i==0:
                compute_unit_description22["arguments"] = ["-i F6VMN7-chain.pftools -o F6VMN7-chain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/chain.map"]
                compute_unit_description22["output"] = "stdout_chain_pftools_final.txt"
                compute_unit_description22["error"] = "stderr_chain_pftools_final.txt"
                print "pftools: part2: chain input: %s" %outdus21[i].list()

            elif i==1:
                compute_unit_description22["arguments"] = ["-i F6VMN7-domain.pftools -o F6VMN7-domain.pftools.ali -l /usr/local/libraries/ethread-lib-latest/pftools/domain.map"]
                compute_unit_description22["output"] = "stdout_domain_pftools_final.txt"
                compute_unit_description22["error"] = "stderr_domain_pftools_final.txt"
                print "pftools: part2: domain input: %s" %outdus21[i].list()

            compute_unit22 = pilotjob2.submit_compute_unit(compute_unit_description22)
            logger.info("PGM-2: Part-2: Finished setup of ComputeDataService. Waiting for scheduling of PD")
            print ("\nPGM-2: Part2: Finished setup of ComputeDataService %s. Waiting for scheduling of PD" % i)

        pilotjob2.wait()
        tot_time = time.time() - start_time
        print "\nPGM-2: Time for part 2: ", tot_time
        logger.info("PGM-2: Terminate Pilot Compute/Data Service")
        print "\nPGM-2: Terminate Pilot Compute/Data Service"
    ###########################################################################################################################
    #***************************************************************************
    #*All the 11 progrmas can be written before the start of the go() function * 
    #*Essentially by copying the pgm_csblast() and modifying it as required    *
    #***************************************************************************

    ########################################################################################################
    # Function to call all the 11 programs in 11 threads
    # thread.join waits for all threads to complete
    # After completion of all threads, ethread function will be called, s3cmd collects the data to local sys
    ########################################################################################################
    def go(self):
        threads=[]
        for i in range(2):
            if i==0:
                thread=threading.Thread(target=self.pgm_csblast)
                thread.start()
            if i==1:
                thread=threading.Thread(target=self.pgm_pftools)
                thread.start()
            #Add remaining programs as threads
            
            threads.append(thread)

        for thread in threads:
            thread.join()
    #########################################################################################################            
        
    ####################################################################################################################
    #ethread starts here
    ####################################################################################################################
    def ethread(self):
        print "\neThread Starts here"
        start_time = time.time()
        output_data_unit_description = { "file_urls": [] }
        output_data_unit = pd.submit_data_unit(output_data_unit_description)
        output_data_unit.wait()

        print "\nethread: Setting PCD-ethread"
        pilot_compute_description_amazon_west = {
                             "service_url": 'ec2+ssh://aws.amazon.com',
                             "number_of_processes": 2,
                             "vm_id": "ami-7b58cc12",
                             "vm_ssh_username":"ubuntu",
                             "vm_ssh_keyname":"ethread",
                             "vm_ssh_keyfile":"/home/cctsg/.ssh/ethread.pem",
                             "vm_type":"t1.micro",
                             "region" :"us-east-1a",
                             "access_key_id":ACCESS_KEY_ID,
                             "secret_access_key":SECRET_ACCESS_KEY
                            }

        pilotjob = pilot_compute_service.create_pilot(pilot_compute_description=pilot_compute_description_amazon_west)

        tot_time = time.time() - start_time
        print "\nethread: Time to set up VM: ", tot_time

        # create compute unit
        start_time = time.time()
        compute_unit_description = {
            "executable": "/bin/date",
            "arguments": [],
            "number_of_processes": 1,
            "output": "stdout_ethread.txt",
            "error": "stderr_ethread.txt",
            "input_data": [input_data_unit.get_url()],
            # Put files stdout.txt and stderr.txt into output data unit
            "output_data": [
                            {
                             output_data_unit.get_url():
                             ["std*"]
                            }
                           ]
        }

        compute_unit = pilotjob.submit_compute_unit(compute_unit_description)
        logger.info("Finished setup of ComputeDataService. Waiting for scheduling of PD")
        print "\neThread: Finished setup of ComputeDataService. Waiting for scheduling of PD"

        compute_unit.wait()
        tot_time = time.time() - start_time
        print "\nTime for eThread part: ", tot_time

        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        print "Input data URL : %s" %input_data_unit.get_url()
        print "Output data URL: %s" %output_data_unit.get_url()
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"


        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "This is url of S3"
        print pd.service_url
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        print "Export S3 data to local"

        e = os.system('s3cmd get -r %s' %pd.service_url)

        if not e == 0:
            print >> sys.stderr, 'Error on fetching data:', e
        #pd.export_du(pd,os.getcwd())
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"


        logger.debug("Output Data Unit: " + str(output_data_unit.list()))
#        print "Output Data Unit: " + str(output_data_unit.list())
    
#        compute_data_service.cancel()
        pilot_data_service.cancel()
        pilot_compute_service.cancel()
        

obj = mainThread()
obj.go()
obj.ethread()
